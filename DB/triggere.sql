
## Triggerele "distante_update" si "adaugare_distante" au aproape aceeasi functionalitate, cu exceptia ca
## trigger-ul "adaugare_distante" se activeaza dupa ce am adaugat o noua distanta in tabelul traseu, iar
## trigger-ul "distante_update" se activeaza dupa ce am modificat ceva in tabelul traseu


DROP TRIGGER IF EXISTS distante_update;

delimiter //
CREATE TRIGGER distante_update after update ON traseu
   	FOR EACH ROW BEGIN
		DECLARE finished INTEGER DEFAULT 0;
		declare distante integer;
        DEClARE distante_cursor CURSOR FOR  SELECT ID_Distanta FROM distante;
		DECLARE CONTINUE HANDLER FOR NOT FOUND SET finished = 1;

	set @ok=1;
	open distante_cursor;
	distante_loop: loop
		fetch distante_cursor into distante;
    
		IF finished = 1 THEN 
		LEAVE distante_loop;
		END IF;
        
        set @oras1 = (select oras1 from distante where id_distanta=distante);
        set @oras2 = (select oras2 from distante where id_distanta=distante);
        
        if((new.oras1=@oras1 and new.oras2=@oras2) or (new.oras2=@oras1 and new. oras1=@oras2)) then
			set @ok=0;
            update distante set distanta=new.`distanta(km)` where
            ((oras1=new.oras1 and oras2=new.oras2) or (oras1=new.oras2 and oras2=new.oras1));
            leave distante_loop;
        end if;
        
        end loop distante_loop;

		if(@ok=1) then
		insert into distante(oras1,oras2,distanta) values (new.oras1,new.oras2,new.`distanta(km)`);
		end if;
        
        
	END; //

delimiter ;







DROP TRIGGER IF EXISTS adaugare_distante;

delimiter //
CREATE TRIGGER adaugare_distante after insert ON traseu
   	FOR EACH ROW BEGIN
		DECLARE finished INTEGER DEFAULT 0;
		declare distante integer;
        DEClARE distante_cursor CURSOR FOR  SELECT ID_Distanta FROM distante;
		DECLARE CONTINUE HANDLER FOR NOT FOUND SET finished = 1;

	set @ok=1;
	open distante_cursor;
	distante_loop: loop
		fetch distante_cursor into distante;
    
		IF finished = 1 THEN 
		LEAVE distante_loop;
		END IF;
        
        set @oras1 = (select oras1 from distante where id_distanta=distante);
        set @oras2 = (select oras2 from distante where id_distanta=distante);
        
        if((new.oras1=@oras1 and new.oras2=@oras2) or (new.oras2=@oras1 and new. oras1=@oras2)) then
			set @ok=0;
            leave distante_loop;
        end if;
        
        end loop distante_loop;

		if(@ok=1) then
		insert into distante(oras1,oras2,distanta) values (new.oras1,new.oras2,new.`distanta(km)`);
		end if;
        
        
	END; //

delimiter ;


## Trigger-ul "adaugare_soferi" : are rolul de a adauga automat angajatii firmei care au "functie" = "sofer", in tabela soferi

DROP TRIGGER IF EXISTS adauga_soferi;

delimiter //

CREATE TRIGGER adauga_soferi after insert ON personal
   	FOR EACH ROW BEGIN
  	if (new.functie='sofer') then
		insert into soferi(ID_Sofer,accidente,nrcurse) values (new.id_personal,0,0);
	end if;
    
	END; //

delimiter ;



## Trigger-ul "update_soferi" : Cand apar modificari in tebala "personal", trebuie sa ne asiguram ca si in tabela "soferi" totul este in regula 


DROP TRIGGER IF EXISTS update_soferi;

delimiter //
CREATE TRIGGER update_soferi BEFORE UPDATE ON personal
   	FOR EACH ROW BEGIN
    
  	if (OLD.functie='sofer' AND NEW.FUNCTIE!='SOFER') then
		DELETE FROM soferi WHERE ID_SOFER=NEW.ID_PERSONAL;
	end if;
    
	if (NEW.FUNCTIE='SOFER' AND OLD.functie!='sofer' ) then
		insert into soferi(ID_Sofer,accidente,nrcurse) values (new.id_personal,0,0);
	end if;
    
	END; //

delimiter ;


## Triggerele "modificari_sejur", respectiv "modificari_sejur_update" au rolul de a modifica tabela "sejur" atunci cand
## este introdus un nou traseu ce tine de respectivul sejur


DROP TRIGGER IF EXISTS modificari_sejur;

delimiter //
CREATE TRIGGER modificari_sejur before insert ON traseu
   	FOR EACH ROW BEGIN

		declare sejur_data_start,sejur_data_sfarsit,sej_start,sej_sfarsit datetime;
        declare durata_zile integer default 0;
        set sejur_data_start = (select data_start from sejur where new.id_sejur=id_sejur);
		set sejur_data_sfarsit = (select data_sfarsit from sejur where new.id_sejur=id_sejur);
        update sejur set distanta_totala=distanta_totala + new.`distanta(km)` where id_sejur=new.id_sejur;
        update sejur set pret_transport=distanta_totala * 0.1 where id_sejur=new.id_sejur;
        
        if(sejur_data_start is null) then
        set sejur_data_start = '2021/12/31'; ## setam data de start la o data foarte mare ca mai apoi la comparatie sa fie luat ce trebuie
        end if;
        
		if(sejur_data_sfarsit is null) then
        set sejur_data_sfarsit = '1999/01/01'; ## setam data de start la o data mica (data curenta) ca mai apoi la comparatie sa fie luat ce trebuie
        end if;
        
        if(sejur_data_start > new.data_start) then
        update sejur set data_start=new.data_start where id_sejur=new.id_sejur;
        end if;
        
        if (sejur_data_sfarsit < new.data_sfarsit) then
        update sejur set data_sfarsit=new.data_sfarsit where id_sejur=new.id_sejur;
        end if;
		
        set sej_start=(select data_start from sejur where id_sejur=new.id_sejur);
        set sej_sfarsit=(select data_sfarsit from sejur where id_sejur=new.id_sejur);
        set durata_zile=timestampdiff(day,sej_start,sej_sfarsit);
		update sejur set durata=durata_zile where id_sejur=new.id_sejur;
        
	END; //

delimiter ;




DROP TRIGGER IF EXISTS modificari_sejur_update;

delimiter //
CREATE TRIGGER modificari_sejur_update before update ON traseu
   	FOR EACH ROW BEGIN

		declare sejur_data_start,sejur_data_sfarsit,sej_start,sej_sfarsit datetime;
        declare durata_zile integer default 0;
        set sejur_data_start = (select data_start from sejur where new.id_sejur=id_sejur);
		set sejur_data_sfarsit = (select data_sfarsit from sejur where new.id_sejur=id_sejur);
        update sejur set distanta_totala=distanta_totala + new.`distanta(km)` - old.`distanta(km)` where id_sejur=new.id_sejur;
        update sejur set pret_transport=distanta_totala * 0.1 where id_sejur=new.id_sejur;
    
        if(sejur_data_start = old.data_start) then
        update sejur set data_start=new.data_start where id_sejur=new.id_sejur;
        end if;
        
        if (sejur_data_sfarsit = old.data_sfarsit) then
        update sejur set data_sfarsit=new.data_sfarsit where id_sejur=new.id_sejur;
        end if;
		
        set sej_start=(select data_start from sejur where id_sejur=new.id_sejur);
        set sej_sfarsit=(select data_sfarsit from sejur where id_sejur=new.id_sejur);
        set durata_zile=timestampdiff(day,sej_start,sej_sfarsit);
		update sejur set durata=durata_zile where id_sejur=new.id_sejur;
		if(old.oras1=new.oras1 or old.oras2=new.oras2) then
        update distante set distanta=new.`distanta(km)` where ((oras1=new.oras1 and oras2=new.oras2) or (oras2=new.oras1 and oras1=new.oras2));
		end if;
        
	END; //

delimiter ;



## Trigger-ul "update_locuri" : are rolul de a modifica numarul de locuri disponibile unui sejur, atunci cand a fost contractata o oferta din acel sejur
## Modifica numarul de locuri disponibile ramase cand introducem o inregistrare in tabela contractare_sejur


DROP TRIGGER IF EXISTS update_locuri;

delimiter //

CREATE TRIGGER update_locuri after insert ON contractare_sejur


   	FOR EACH ROW BEGIN
    
		update sejur set nr_locuri=nr_locuri-new.nrsejururi where id_sejur=new.id_sejur;
       
	END; //

delimiter ;

