-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema agentie_turism
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema agentie_turism

CREATE SCHEMA IF NOT EXISTS `agentie_turism` DEFAULT CHARACTER SET utf8 ;
USE `agentie_turism` ;
-- -----------------------------------------------------
-- Table `agentie_turism`.`departament`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `agentie_turism`.`departament` (
  `ID_Departament` INT(11) NOT NULL AUTO_INCREMENT,
  `Denumire` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`ID_Departament`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;
-- -----------------------------------------------------
-- Table `agentie_turism`.`sediu`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `agentie_turism`.`sediu` (
  `ID_Sediu` INT(11) NOT NULL AUTO_INCREMENT,
  `Oras` INT(11) NOT NULL,
  `Denumire` VARCHAR(45) NOT NULL,
  `Adresa` VARCHAR(200) NULL,
  `Telefon` VARCHAR(45) NULL,
  PRIMARY KEY (`ID_Sediu`),
   INDEX `fk_Sediu_Orase_idx` (`Oras` ASC),
   
  CONSTRAINT `fk_Sediu_Orase`
    FOREIGN KEY (`Oras`)
    REFERENCES `agentie_turism`.`orase` (`ID_Oras`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
    
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `agentie_turism`.`personal`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `agentie_turism`.`personal` (
  `ID_Personal` INT(11) NOT NULL AUTO_INCREMENT,
  `Nume` VARCHAR(45) NOT NULL,
  `Prenume` VARCHAR(45) NOT NULL,
  `Adresa` VARCHAR(200) NULL,
  `Telefon` VARCHAR(45) NULL,
  `Functie` VARCHAR(45) NOT NULL,
  `Salariu` FLOAT NOT NULL DEFAULT 0,
  `Data_angajarii` DATE NULL,
  `ID_Sediu` INT(11) NOT NULL,
  `ID_Departament` INT(11) NOT NULL,
  PRIMARY KEY (`ID_Personal`),
  INDEX `fk_Personal_Sediu_idx` (`ID_Sediu` ASC),
  INDEX `fk_Personal_Departament_idx` (`ID_Departament` ASC),
  
  CONSTRAINT `fk_Personal_Departament`
    FOREIGN KEY (`ID_Departament`)
    REFERENCES `agentie_turism`.`departament` (`ID_Departament`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
    
  CONSTRAINT `fk_Personal_Sediu`
    FOREIGN KEY (`ID_Sediu`)
    REFERENCES `agentie_turism`.`sediu` (`ID_Sediu`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
    
    
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `agentie_turism`.`concedii`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `agentie_turism`.`concedii` (
  `ID_Concediu` INT(11) NOT NULL AUTO_INCREMENT,
  `ID_Personal` INT(11) NOT NULL,
  `Data_start` DATE NOT NULL,
  `Data_sfarsit` DATE NOT NULL,
  PRIMARY KEY (`ID_Concediu`),
  INDEX `fk_Concedii_Personal_idx` (`ID_Personal` ASC),
  
  CONSTRAINT `fk_Concedii_Personal`
    FOREIGN KEY (`ID_Personal`)
    REFERENCES `agentie_turism`.`personal` (`ID_Personal`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
    
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `agentie_turism`.`autocare`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `agentie_turism`.`autocare` (
  `ID_Autocar` INT(11) NOT NULL AUTO_INCREMENT,
  `Marca` VARCHAR(45) NOT NULL,
  `Model` VARCHAR(45) NOT NULL,
  `Data_Achizitiei` DATE NOT NULL,
  `Numar_Locuri` INT(11) NULL DEFAULT 0,
  `Km_Parcursi` INT(11) NULL DEFAULT 0,
  `Descriere_tehnica` VARCHAR(500) NULL DEFAULT NULL,
  `ID_Sediu` INT(11) NOT NULL,
  PRIMARY KEY (`ID_Autocar`),
	INDEX `fk_Autocare_Sediu_idx` (`ID_Sediu` ASC),
    
  CONSTRAINT `fk_Autocare_Sediu`
    FOREIGN KEY (`ID_Sediu`)
    REFERENCES `agentie_turism`.`sediu` (`ID_Sediu`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
    
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `agentie_turism`.`tip_oras`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `agentie_turism`.`tip_oras` (
  `ID_Tip` INT(11) NOT NULL AUTO_INCREMENT,
  `Tipul` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`ID_Tip`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------
-- Table `agentie_turism`.`orase`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `agentie_turism`.`orase` (
  `ID_Oras` INT(11) NOT NULL AUTO_INCREMENT,
  `Denumire` VARCHAR(45) NOT NULL,
  `Tara` VARCHAR(45) NOT NULL,
  `ID_Tip` INT(11) NULL DEFAULT NULL,
  `Descriere` VARCHAR(1500) NULL DEFAULT NULL,
  PRIMARY KEY (`ID_Oras`),
  INDEX `fk_Orase_Tip_Oras_idx` (`ID_Tip` ASC),
  CONSTRAINT `fk_Orase_Tip_Oras`
    FOREIGN KEY (`ID_Tip`)
    REFERENCES `agentie_turism`.`tip_oras` (`ID_Tip`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `agentie_turism`.`hoteluri`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `agentie_turism`.`hoteluri` (
  `ID_Hotel` INT(11) NOT NULL AUTO_INCREMENT,
  `Nume` VARCHAR(45) NOT NULL,
  `Oras` INT(11) NOT NULL,
  `Descriere` VARCHAR(2000) NULL DEFAULT NULL,
  `Facilitati` VARCHAR(2000) NULL,
  `NrStele` INT(11) NULL DEFAULT 0,
  `Pret_pers_noapte` FLOAT DEFAULT 0,
  `Adresa` VARCHAR(100) NULL,
  `Telefon` VARCHAR(45) NULL,
  `Pagina_Web` VARCHAR(200) NULL,
  PRIMARY KEY (`ID_Hotel`),
  INDEX `fk_Hoteluri_Orase_idx` (`Oras` ASC),
  CONSTRAINT `fk_Hoteluri_Orase`
    FOREIGN KEY (`Oras`)
    REFERENCES `agentie_turism`.`orase` (`ID_Oras`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------
-- Table `agentie_turism`.`rezervare_hotel`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `agentie_turism`.`rezervare_hotel` (
  `ID_Rezervare` INT(11) NOT NULL AUTO_INCREMENT,
  `ID_Contract` INT(11) NOT NULL,
  `ID_Hotel` INT(11) NULL,
  `Data_start` DATETIME NULL DEFAULT NULL,
  `Data_sfarsit` DATETIME NULL DEFAULT NULL,
  `Pret` FLOAT default 0,
  `Durata` INT NULL DEFAULT 0,
  PRIMARY KEY (`ID_Rezervare`),
  INDEX `fk_Cazare_Hoteluri_idx` (`ID_Hotel` ASC),
  INDEX `fk_rezervare_hotel_contract_idx` (`ID_Contract` ASC),
  CONSTRAINT `fk_Cazare_Hoteluri`
    FOREIGN KEY (`ID_Hotel`)
    REFERENCES `agentie_turism`.`hoteluri` (`ID_Hotel`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `fk_rezervare_hotel_contract`
    FOREIGN KEY (`ID_Contract`)
    REFERENCES `agentie_turism`.`contractare_sejur` (`ID_Contract`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `agentie_turism`.`utilizatori`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `agentie_turism`.`utilizatori` (
  `ID_Utilizator` INT(11) NOT NULL AUTO_INCREMENT,
  `Username` VARCHAR(45) NOT NULL,
  `Parola` VARCHAR(45) NOT NULL,
  `Email` VARCHAR(45) NOT NULL,
  `Nume` VARCHAR(45) NOT NULL,
  `Prenume` VARCHAR(45) NOT NULL,
  `Adresa` VARCHAR(45) NULL,
  `Telefon` VARCHAR(45) NULL,
  `Sex` VARCHAR(2) NOT NULL,
  `Puncte_Fidelitate` INT(11) NULL DEFAULT 0,
  `Admin` VARCHAR(2),
  PRIMARY KEY (`ID_Utilizator`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------
-- Table `agentie_turism`.`soferi`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `agentie_turism`.`soferi` (
  `ID_Sofer` INT(11) NOT NULL,
  `Accidente` INT(11) NULL DEFAULT 0,
  `NrCurse` INT(11) NULL DEFAULT 0,
  PRIMARY KEY (`ID_Sofer`),
  INDEX `fk_Soferi_Personal_idx` (`ID_Sofer` ASC),
  
  CONSTRAINT `fk_Soferi_Personal`
    FOREIGN KEY (`ID_Sofer`)
    REFERENCES `agentie_turism`.`personal` (`ID_Personal`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
    
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `agentie_turism`.`curse`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `agentie_turism`.`sejur` (
  `ID_Sejur` INT(11) NOT NULL AUTO_INCREMENT,
  `ID_Autocar` INT(11) NULL,
  `Sofer1` INT(11) NULL,
  `Sofer2` INT(11) NULL,
  `Ghid` INT(11) NULL,
  `Data_start` DATETIME NULL DEFAULT NULL,
  `Data_sfarsit` DATETIME NULL DEFAULT NULL,
  `Distanta_totala` INT(11) NULL DEFAULT 0,
  `Pret_transport` FLOAT NULL,
  `Nr_locuri` INT DEFAULT 0,
  `Durata` INT DEFAULt 0,
  PRIMARY KEY (`ID_Sejur`),
  UNIQUE INDEX `ID_Sejur_UNIQUE` (`ID_Sejur` ASC),
  INDEX `fk_sejur_Autocare_idx` (`ID_Autocar` ASC),
  INDEX `fk_sejur_soferi1_idx` (`Sofer1` ASC),
  INDEX `fk_sejur_soferi2_idx` (`Sofer2` ASC),
  INDEX `fk_sejur_personal_idx` (`Ghid` ASC),
  CONSTRAINT `fk_sejur_Autocare`
    FOREIGN KEY (`ID_Autocar`)
    REFERENCES `agentie_turism`.`autocare` (`ID_Autocar`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `fk_sejur_soferi1`
    FOREIGN KEY (`Sofer1`)
    REFERENCES `agentie_turism`.`soferi` (`ID_Sofer`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `fk_sejur_soferi2`
    FOREIGN KEY (`Sofer2`)
    REFERENCES `agentie_turism`.`soferi` (`ID_Sofer`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `fk_sejur_ghid`
    FOREIGN KEY (`Ghid`)
    REFERENCES `agentie_turism`.`personal` (`ID_Personal`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `agentie_turism`.`contractare_sejur`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `agentie_turism`.`contractare_sejur` (
  `ID_Contract` INT(11) NOT NULL AUTO_INCREMENT,
  `ID_Sejur` INT(11) NULL,
  `ID_Utilizator` INT(11) NULL,
  `Pret_pers` FLOAT default 0,
  `NrSejururi` INT(11) NULL,
  `Reduceri` FLOAT NULL DEFAULT 0,
  `Pret_Final` FLOAT default 0,
  PRIMARY KEY (`ID_Contract`),
  INDEX `fk_Contractare_Sejur_utilizatori_idx` (`ID_Utilizator` ASC),
  INDEX `fk_Contractare_Sejur_Sejur_idx` (`ID_Sejur` ASC),
  CONSTRAINT `fk_Contractare_Sejur_utilizatori`
    FOREIGN KEY (`ID_Utilizator`)
    REFERENCES `agentie_turism`.`utilizatori` (`ID_Utilizator`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Contractare_Sejur_Sejur`
    FOREIGN KEY (`ID_Sejur`)
    REFERENCES `agentie_turism`.`sejur` (`ID_Sejur`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `agentie_turism`.`obiective_turistice`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `agentie_turism`.`obiective_turistice` (
  `ID_Obiectiv` INT(11) NOT NULL AUTO_INCREMENT,
  `ID_Oras` INT(11) NOT NULL,
  `Denumire` VARCHAR(45) NULL,
  `Descriere` VARCHAR(1000) NULL,
  PRIMARY KEY (`ID_Obiectiv`),
  INDEX `fk_Obiective_Turistice_Orase_idx` (`ID_Oras` ASC),
  CONSTRAINT `fk_Obiective_Turistice_Orase`
    FOREIGN KEY (`ID_Oras`)
    REFERENCES `agentie_turism`.`orase` (`ID_Oras`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------
-- Table `agentie_turism`.`traseu`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `agentie_turism`.`traseu` (
  `ID_Traseu` INT(11) NOT NULL AUTO_INCREMENT,
  `ID_Sejur` INT(11) NOT NULL,
  `Oras1` INT(11) NULL,
  `Oras2` INT(11) NULL,
  `Data_start` DATETIME NULL,
  `Data_sfarsit` DATETIME NULL,
  `Distanta(km)` INT NULL,
  PRIMARY KEY (`ID_Traseu`),
  INDEX `fk_Traseu_orase1_idx` (`Oras1` ASC),
  INDEX `fk_Traseu_orase2_idx` (`Oras2` ASC),
  INDEX `fk_Traseu_sejur_idx` (`ID_Sejur` ASC),
  CONSTRAINT `fk_Traseu_orase1`
    FOREIGN KEY (`Oras1`)
    REFERENCES `agentie_turism`.`orase` (`ID_Oras`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Traseu_orase2`
    FOREIGN KEY (`Oras2`)
    REFERENCES `agentie_turism`.`orase` (`ID_Oras`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Traseu_sejur`
    FOREIGN KEY (`ID_Sejur`)
    REFERENCES `agentie_turism`.`sejur` (`ID_Sejur`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;
-- -----------------------------------------------------
-- Table `agentie_turism`.`distante`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `agentie_turism`.`distante` (
  `ID_Distanta` INT(11) NOT NULL AUTO_INCREMENT,
  `Oras1` INT(11) NOT NULL,
  `Oras2` INT(11) NOT NULL,
  `Distanta` INT NULL DEFAULT 0,
  PRIMARY KEY (`ID_Distanta`),
  INDEX `fk_Distante_orase1_idx` (`Oras1` ASC),
  INDEX `fk_Distante_orase2_idx` (`Oras2` ASC),
  CONSTRAINT `fk_Distante_orase1`
    FOREIGN KEY (`Oras1`)
    REFERENCES `agentie_turism`.`orase` (`ID_Oras`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Distante_orase2`
    FOREIGN KEY (`Oras2`)
    REFERENCES `agentie_turism`.`orase` (`ID_Oras`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;