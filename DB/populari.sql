########### AICI SUNT POPULARILE TABELELOR CREATE ########### 
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

### Inserarile in tabela tip_oras pentru tipurile oraselor care exista ###

INSERT INTO tip_oras(tipul) VALUES ('plecare'),('litoral'),('montan'),('cultural');

-- -------------------------------------------------------------------------------------------

### Inserarile in tabela Orase pentru orasele existente ###

INSERT INTO Orase (Denumire,Tara,ID_Tip,Descriere) VALUES
('Cluj-Napoca','Romania',1,'Oras de plecare. Plecarile se vor efectua din Piata Unirii la o anumita ora specificata'),
('Bucuresti','Romania',1,'Oras de plecare. Plecarile vor avea loc din Piata Romana la o anumita ora specificata'),
('Iasi','Romania',1,'Oras de plecare. Plecarile vor avea loc din Parcul Copou la o anumita ora specificata'),
('Timisoara','Romania',1,'Oras de plecare. Plecarile se vor efectua din Parcul Rozelor la o anumita ora specificata'),
('Brasov','Romania',3,'Brasovul este unul dintre cele mai indragite si frumoase orase din Romania. Fiind situat la poalele granitei dintre Carpatii de Curbura si cei Meridionali, Brasovul este o excelenta solutie in a va petrece un concediu de neuitat!'),
('Sibiu','Romania',4,'Sibiul este un oras inflorit, unul dintre orasele ce prezinta un nivel ridicat al vietii culturale, asta si datorita faptului ca in anul 2007 a fost Capitala Culturala Europeana.'),
('Sofia','Bulgaria',4,'Sofia, capitala Bulgariei, este o excelenta alternativa de a va petrece concediul mult dorit. Avand undeva la un milion de locuitori, acest oras perla al Bulgariei va ofera o multime de posibilitati atunci cand vreti sa aveti un sejur din plin reusit.'),
('Atena','Grecia',4,'Atena, capitala Greciei, are un impact deosebit atunci cand vine vorba de culturalism. Avand un istoric si un trecut extrem de interesant, aici puteti intalni o multitudine de obiective turistice ce dateaza inca din vremea antichitatii.'),
('Creta','Grecia',2,'Creta, una dintre cele mai populare insule din lume, este o ocazie perfecta de a va petrece un sejur estival care sa va ofere din plin confortul pe care vi-l dorit. Fiind una din optiunile preferate ale romanilor si nu numai, Insula Creta este de departe cea mai frumoasa insula a Greciei si o alegere perfecta.'),
('Budapesta','Ungaria',4,'Budapesta, capitala Ungariei, este poate cel mai frumos oras din estul Europei. Avand o puternica influenta culturala, acest oras se numara printre locurile pe care neaparat trebuie sa le vizitezi cel putin o data!'),
('Viena','Austria',4,'Una dintre cele mai renumite capitale din Europa, Viena este un puternic pol de cultura situat in partea central-estica a Europei. Strabatuta de Dunare, aici puteti regasi puternicele influente habsburgice care se regasesc si astazi, dupa mai bine de 100 de ani.'),
('Innsbruck','Austria',3,'Unul dintre cele mai renumite orase montane, acest oras austriac permite turistilor pasionati de munte si de sporturile de iarna, sa isi petreaca o vacanta asa cum isi doresc ei!'),
('Roma','Italia',4,'Roma, capitala Italiei, este o optiune perfecta atunci cand vreti sa vizitati unul dintre cele mai vechi orase de pe planeta. Cu multe obiective ce dateaza inca din Imperiul Roman, aici puteti admira o gramada de obiective turistice, cum ar fi, de exemplu, Colosseumul, dar si altele!.'),
('Berlin','Germania',4,'Berlin, capitala Germaniei, impresioneaza prin numeroasele sale obiective turistice, dar si prin istoria sa recenta petrecuta in perioada celui de Al Doilea Razboi Mondial. Cu siguranta, cea mai reprezentativa imagine a Berlinului este Zidul Berlinului, construit dupa incheierea razboiului pentru a separa Estul de Vest.'),
('Paris','Franta',4,'Paris, capitala Frantei, este printre cele mai alese orase de catre turistii din intreaga lume. Avand o splendoare estetica cum nu se mai poate intalni niciunde in lume, Parisul impresioneaza prin numeroasele sale obiective turistice, precum: Turnul Effel, muzeul Louvre, biserica Notre-Dame, arcul de triumf, dar si multe altele.'),
('Monte Carlo','Monaco',2,'Monte Carlo, capitala statului Monaco, este probabil locul unde cu totii si-ar dori sa traiasca sau sa fie prezenti macar o data in viata. Situat pe Coasta de Azur, la malul Marii Mediterane, Monte-Carlo reprezitna optiunea ideala pentru acei turisti care vor sa bage adanc manca in buzunar si sa petreaca o vacanta de neuitat in cel mai bogat oras de pe planeta!'),
('Londra','Marea Britanie',4,'Londra, capitala Regatului Unit, este un "must-have" pentru orice turist. Infloritor, frumos din puncte de vedere estetic, acest oras va ofera posibilitatea de a vedea o gramada de obiective turistice ce dateaza de mai bine de cateva secole.'),
('Madrid','Spania',4,'Madrid, capitala Spaniei, este unul dintre cele mai "vanate" orase, nu numai pentru obiectivele sale turistice, dar si pentru bogatul folclor sportiv-fotbalistic pe care-l detine acest oras!');

-- -----------------------------------------------------------------------------------------

### Inserarile in tabela Obiective_Turistice ###

INSERT INTO Obiective_Turistice (ID_Oras,Denumire,Descriere) VALUES
(5,'Biserica Neagra','Biserica Neagra este biserica parohiala a comunitatii evanghelice luterane din Brasov, situata in centrul municipiului Brasov. Cladirea gotica a fost partial avariata in incendiul din 1689, cand zidurile ei s-au inegrit si a primit numele actual.'),
(5,'Cetatea Brasovului','Cetatea se afla pe varful Tampei si a constituit in evul mediu cel mai important punct de aparare pentru Brasov. Ansamblul fortificat ocupa o suprafata de aproximativ 23.000 mp, cu ziduri inalte si groase de 1,70 pana la 1,80 m.'),
(5,'Partiile Brasovului','Brasovul este renumit pentru partiile din proximitatea orasului. Peste 20 de domenii pentru practicat sporturi de iarna va stau la dispozitie intr-un peisaj montan de exceptie.'),
(5,'Castelul Bran','Castelul Bran este un monument istoric si arhitectonic, situat in Pasul Bran-Rucar, la 30 de kilometri de Brasov. El adaposteste in acest moment muzeul Bran, muzeu ce se intinde pe cele 4 etaje ale castelului. La muzeu sunt expuse colectii de ceramica, mobilier, arme si armuri, iar in curtea castelului se afla un mic muzeu al satului, cu case traditionale din regiunea culoarului Rucar-Bran.'),
(6,'Piata Mare','Piata Mare este cea mai renumita zona din orasul Sibiu. Situata fix in centrul orasului, pe aceasta pietonala puteti sa va relaxati pe terasele din apropiere, sau sa vizitati cateva obiective importante: Turnul Sfatului sau Muzeul Brukenthal'),
(7,'Palatul Regal din Sofia','Aceasta cladire, impunatoare din toate punctele de vedere, este cea care a fost construita initial de fortele politice Otomane, fiind locul in care eroul national Vasili Levski a fost torturat inainte de marea sa executie din 1873.'),
(7,'Muzeul Icoanelor din Sofia','Aceasta colectie de imagini sacra este cea mai importanta din Bulgaria si trebuie vizitata de iubitorii de arte religioase. Icoanele aflate aici dateaza din secolul 13, pana in secolul 19'),
(8,'Muzeul Acropole','Aici, se gasesc colectii de artefacte descoperite in cursul sapaturilor arheologice pe Acropole. Si tot aici spera grecii sa vada, candva, expuse, pentru totdeauna, celebrele "marmuri ale Partenonului" sau "marmurile Elgin", desprinse de pe zidurile Partenonului si ale altor cladiri de pe Acropole intre anii 1801-1812 si transportate in Anglia, din ordinul lordului Elgin, ambasador al Marii Britanii la Sublima Poarta'),
(8,'Muzeul National Arheologic','Dintre muzeele Atenei, acesta e cel mai mare, cel mai cunoscut si, din cauza acesta, cel mai aglomerat; cu turisti, evident. Ce-i drept, ar fi pacat sa nu-l vezi, caci cuprinde o multitudine coplesitoare de opere de arta si tehnica ale preistoriei si Antichitatii Europei: o adevarata padure de statui splendide, din marmura si bronz'),
(9,'Creta Centrala: Heraklion','Este o zona cu plaje frumoase, plantatii de vita-de-vie si maslini, unde se afla majoritatea siturilor arheologice care constituie leaganul civilizatiei minoice.'),
(9,'Creta de Vest: Chania, Rethimno','Creta de vest este o zona mai putin modernizata, unde se gasesc sate izolate cu traditii bine pastrate. Statiunile, aglomerate vara, au plaje frumoase si mici golfulete.'),
(10,'Podul cu Lanturi','Podul cu lanturi a devenit unul dintre cele mai cunoscute obiective turistice din Budapesta. Podul face legatura peste Dunare intre Pesta si Buda. Ofera una dintre cele mai frumoase privelisti din oras, cu toata maretia Dunarii ce isi gaseste drumul pe sub acest pod.'),
(10,'Palatul Regal','Palatul Regal a fost distrus si reconstruit de multe ori. Regele Bela al IV-lea a inceput constructia palatului in secolul al XIII-lea dupa invazia mongolilor. Palatul original a fost construit in stil gotic si a continuat sa fie extins timp de 300 de ani. Epoca de aur a palatului a fost in timpul domniei regelui Matthias. A fost total distrus in 1686 cand armata Habsburgilor a eliberat Buda de sub ocupatia Otomanilor'),
(11,'Palatul Schonbrunn','Palatul Schonbrunn trebuia sa fie Versailles-ul Imperiului Austriac insa, in anul 1695 atunci cand Fischer von Erlach a proiectat palatul, costurile pentru ridicarea sa erau prea mari. Astfel, palatul a fost conceput initial mai mult ca resedinta de vara a Habsburgilor dar incepand cu Maria Tereza (1717-1780) a inceput si epoca de aur a Habsburgilor, ea alegandu-si ca resedinta Palatul Schonbrunn.'),
(11,'Palatul Belvedere','Palatul Belvedere se afla in mijlocul unui parc splendid. Palatul a fost construit pentru Printul Eugen de Savoia, de catre J.L. von Hildebrandt, un foarte cunoscut arhitect baroc al perioadei respective. Belvedere inseamna un loc din care se poate vedea frumusetea iar Palatul Belvedere ofera o priveliste minunata a orasului vechi din gradinile sale'),
(11,'Catedrala Sf. Stefan','Catedrala Sf Stefan, obiectiv turistic important, a supravietuit multor razboaie si a devenit simbolul libertatii Vienei. Cladirea in stil gotic a fost construita in anul 1147 dar acoperisul sau din tigla sub forma de diamant a fost adaugat in anul 1952. Catedrala Sf Stefan a fost subiectul multor carti, poze si studii. Unicitatea catedralei precum si multitudinea de detalii fac din catedrala Sf Stefan, o comoara a arhitecturii. Altarele, arcadele, turnurile, imaginile: fiecare detaliu are scopul si istoria sa'),
(12,'Schloss Ambras','Este un castel minunat in Innsbruck, vizibil cam din orice latura a lui. Are o poveste interesanta pe care merita sa o ascultati la teleghid. Veti gasi si tablou cu un portret al lui Vlad Tepes'),
(12,'Fabrica de cristale Swarovski','Un must see. Se poate ajunge relativ usor cu un autobuz, cursa speciala care vine destul de des. Este si teleghid in limba romana! In muzeul cristalelor sunt diverse spectacole interesante.'),
(12,'Partiile din Innsbruck','Supranumit si mall-ul alpin, domeniul schiabil Innsbruck este o bijuterie inconjurata de munti impozanti si reprezinta o alegere perfecta pentru pasionatii de schi si snowboarding. Un concediu in regiunea de schi Innsbruck, inseamna o experienta de iarna de neuitat. Regiunea de schi din Innsbruck este formata din mai multe statiuni: Axamer Lizum, Glunzeger, Patscherkofel, Seegrube, Schlick si a Ghetarul Stubai.'),
(13,'Colosseumul','Coloseumul este probabil cea mai impresionanta cladire a Imperiului Roman. Cunoscut initial sub numele de Amfiteatrul Flavian, Coloseumul era cea mai mare cladire a epocii. Structura monumentala este astazi o ruina, dar chiar si asa este un obiectiv turistic foarte frumos si impozant. Imparatul Vespasian, primul din dinastia Flaviana, a inceput constructia Coloseumului in anul 72 d. Ch. Constructia a fost terminata anul 80 d.Ch. la un an dupa moartea lui Vespasian.'),
(13,'Basillica San Pietro','Aceasta cladire mareata este centrul crestinismului. Opulenta interiorului sau sta marturie pentru bogatia bisericii catolice din secolul al XVI-lea. Imparatul Constantin, primul imparat crestin al Romei, a ordonat construirea Basilicii Sf. Petru pe dealul Vaticanului. Locatia a fost aleasa simbolic, pe locul in care Sfantul Petru, unul dintre apostoli, a fost ingropat in anul 64 d. Ch. Un mic altar exista deja in locul respectiv, dar acesta a fost inlocuit cu biserica ce a fost finalizata in anul 349 d.Ch.'),
(13,'Columna lui Traian','Columna lui Traian a fost ridicata intre anii 106-113 d.C. pentru a onora victoriile imparatului Traian in Dacia. Aceasta columna era situata in forumul lui Traian (forum ce abia fusese inaugurat in acele vremuri) si inconjurata de cladiri. O spirala de reliefuri este gravata de jur imprejurul coloanei. Banda de reliefuri are o lungime de peste 180 de metri iar latimea sa variaza de la 60 de cm jos pana la 120 de cm in varf. Pe columna exista peste 2000 de personaje gravate in scene ce descriu povestea celor doua razboaie romane din Dacia'),
(14,'Poarta Brandenburg','Brandenburger Tor, Poarta Brandenburg, este o poarta monumentala construita in secolul al 18-lea ca simbol al pacii. In timpul Razboiului Rece, cand poarta se afla chiar pe granita dintre Est si Vest, a devenit un simbol al unui oras divizat. De la Caderea Zidului Berlinului, Poarta Brandenburg a devenit simbolul Berlinului reunificat'),
(14,'Zidul Berlinului','Zidul Berlinului, care a despartit orasul in doua parti, estica si vestica, a fost simbolul Razboiului Rece. Construit de guvernul Republicii Democrate Germane pentru a impiedica pe est germani sa fuga spre Germania Federala, marea parte a acestuia a fost demolata de cand granita dintre est si vest a fost deschisa in 1989. Dupa al doilea razboi mondial, Germania a fost impartita in patru zone: una americana, britanica, franceza si sovietica'),
(14,'Parlamentul','Reichstag, sediul Parlamentului German este unul dintre cele mai importante obiective turistice si istorice din Berlin. Cladirea Parlamentului este in apropiere de Poarta Brandenburg si inainte de unificarea Germaniei se afla chiar in apropiarea Zidului Berlinului. Dupa fondarea Imperiului German in 1872, a aparut o nevoie pentru o cladire mai mare pentru Parlamentul din Berlin. Aveau sa mai treaca inca 10 ani pana cand s-a putut ajunge la un acord asupra proiectului.'),
(15,'Turnul Eiffel','Daca mergi in Paris trebuie neaparat sa vizitezi Turnul Eiffel. Sa mergi in Paris si sa nu vizitezi Turnul Eiffel este pur si simplu un lucru de neconceput. De fapt aceasta atractie a Parisului poate fi vazuta din aproape orice punct al orasului. Din Turnul Eiffel nu vei avea parte de cea mai tare priveliste din Paris dar este in mod cert un loc in care trebuie sa te duci din cauza arhitecturii sale si a grandorii. Monumentul isi trage numele de la designer-ul sau Gustave Eiffel si este cea mai inalta cladire din Paris.'),
(15,'Castelul Versaiiles','Palatul Versailles este unul dintre cele mai mari si opulente castele din intreaga lume. Cu peste 2100 de ferestre, 1250 de seminee si 67 de scari interioare, castelul Versailles este una dintre cele mai vizitate atractii turistice din Franta. Fiind un exemplu perfect de arta franceza a secolului al 18-lea, aceasta cladire istorica se afla pe lista Patrimoniului Universal UNESCO de mai bine de 30 de ani. Castelul a fost resedinta oficiala pana in anul 1789, iar de atunci a fost privit ca o resedinta neoficiala.'),
(15,'Muzeul Luvru','Muzeul Luvru este cel mai vizitat muzeu de arta din lume, un monument plin de incarcatura istorica si un muzeu national al Frantei. Este un obiectiv turistic central din Paris fiind situat in arondismentul 1 al orasului. In cadrul muzeului Luvru sunt expuse in jur de 35000 de obiecte de arta din mileniul 6 I.Ch. si pana in secolul al XIX-lea D.Ch. Muzeul este gazduit de palatul Louvre care initial a fost conceput ca o fortareata in secolul al XII-lea sub domnia lui Filip al II-lea.'),
(15,'Notre Dame','Notre Dame nu este chiar cea mai mare catedrala din lume, dar cu siguranta este cea mai faimoasa. Capodopera gotica este localizata in Ile de la Cite, o mica insula in inima orasului. Episcopul Parisului Maurice de Sully a inceput constructia in 1163. Catedrala trebuia construita in noul stil gotic dorindu-se sa reflecte statutul Parisului de capitala a regatului francez. A fost prima catedrala construita la o scara uriasa devenind astfel prototipul pentru catedralele ce s-au construit in Franta in perioada urmatoare.'),
(16,'Palatul Princiar','Vizitează Palatul Princiar, cu o structură care datează din anul 1191 și servește ca reședință regală. Acesta poate fi vizitat după un orar strict satbilit în funcție de anotimp. Cea mai interesantă este Sala Tronului, dar la fel de frumnoase sunt și apartamentele regale. Veți găsi aici o combinație îndrăzneață de marmură, brocart și mahon. Totul  este foarte impunător și atrăgător.'),
(16,'Catedrala Monaco','Vizitează Catedrala Monaco din apropierea Palatului Princiar, unde se află și mormântul Prințesei Grace de Monaco, fosta actriță Grace Kelly, unul dintre cele mai iubite personaje monarhice din secolul XX și în același timp una din cele mai reprezentative actrițe ale acelui secol.'),
(17,'Big Ben','Big Ben este unul dintre cele mai importante obiective turistice in Londra, devenind aproape o emblema a capitalei britanice. Big Ben arata de-a dreptul incantator noaptea cand fatada si cesurile de pe fiecare fata sunt iluminate. Atunci cand Parlamentul se afla in sesiune o lumina straluceste peste fatada ceasului. Cadranele ceasului au o suprafata de peste doi metri patrati, iar minutarul are peste 4 metri. Big Ben are un pendul urias care este reglat de un sac de monezi si este un ceas excelent care s-a oprit foarte rar.'),
(17,'Palatul Buckingham','Palatul Buckingham (Buckingham Palace) este resedinta oficiala din Londra a suveranului Regatului Unit al Marii Britanii si Irlandei de Nord si a fost deschis pentru prima pentru publicul larg in anul 1993. Intrarea la Palatul Buckingham se face prin Curtea Ambasadorilor de pe strada Buckingham Palace. Sala Mare a Palatului este locul in care se afla vechea sala de primire la Palatul Buckingham. Aceasta sala este dominata de scarile impozante si de balustrada din bronz cu ornamente florale.'),
(17,'Tower Bridge','Acum mai bine de 100 de ani, in epoca Victoriana a fost construit acest pod care a devenit unul dintre cele mai importante si cunoscute obiective turistice din Londra. Pasaje pietonale au fost facut pentru a permite oamenilor sa traverseze Tamisa in timp ce podul poate fi ridicat pentru a permite vapoarelor mari sa treaca. Astazi, pasajele pietonale servesc drept galerii de unde poate fi admirata panorama peste Tamisa; una dintre cele mai incantatoare privelisti ale Londrei, un oras al carui colorit este intr-o permanenta schimbare.'),
(17,'Parlamentul','Casele Parlamentului - Parlamentul este o cladire impunatoare de pe malul Tamisei si un obiectiv turistic important in Londra cunoscut si sub numele de Palatul Westminster. Cladirea este situata pe locul in care Edward Confesorul a poruncit construirea palatului original in prima jumatate a secolului al XI-lea. In 1547 rezistenta monarhista s-a mutat la Palatul Whitehall, dar Lorzii au continuat sa se intalneasca la Westminster, in timp ce Camera Comunelor se reunea la Capela Sf. Stefan.'),
(18,'Palatul Regal','Palatul Regal sau Palacio Real este cel mai mare si cu siguranta unul dintre cele mai impresionante palate din Europa. Are peste 2000 de camere decorate in modul cel mai luxos cu putinta, dintre care 50 pot fi vizitate. Este situat in estul centrului istoric al orasului Madrid si destul de aproape de Plaza de Espana.'),
(18,'Puerta del Sol','Plaza Puerta del Sol este punctul perfect de pornire in explorarea orasului Madrid. Aceasta piata aglomerata, central situata este unul dintre locurile cele mai pline de viata din oras. Acum Puerta del Sol este inima centrului istoric din Madrid, dar in trecut locul era la marginea din partea de est a orasului. Numele pietei provine de la poarta de est a orasului ce exista aici in secolul al 15-lea. Poarta era probabil numita asa dupa soarele desenat pe ea - Puerta del Sol sau Poarta Soarelui.'),
(18,'Arco de la Victoria','Teatrul lui Dionisos, construit in scobitura naturala in sudul Acropolelor, a fost primul teatru din lume construit din piatra si reprezinta nasterea tragediei grecesti. Original locul unde era venerat zeul Dionisos, zeul vinului si al fertilitatii, in dans si cantec in sec al V-lea, piese ca Aeschylus, Sofocles, Euripides si Aristofan au fost jucate aici. Teatrul a fost construit in mai mute etape, fiecare contribuind la dezvoltarea dramei, in sec al IV-lea d. Ch fiind refacut cu scaune de marmura.'),
(18,'Catedrala Almudena','Catedrala Almudena din Madrid a avut nevoie de mai bine de 100 de ani pentru a fi construita. A fost in cele din urma sfintita de Papa Ioan Paul al II-lea in anul 1993. La putin timp dupa ce Regele Filip al II-lea a facut Madrid capitala Spaniei in anul 1561, a dorit sa construiasca o catedrala pentru noua capitala. Din cauza turbulentelor politice si opozitie puternice din partea arhiepiscopiei orasului Toledo, care atunci era un oras mult mai mare, constructia a tot fost amanata. In cele din urma, in anul 1868 o congregatie devotata Fecioarei Almudena, sfantul care patroneaza Madridul, a primit permisiunea arhiepiscopiei din Toledo pentru a construi o noua biserica dedica sfantei patroane a orasului.');

-- ---------------------------------------------------------------------------------------------------

### Inserarea in tabela Hoteluri ###

INSERT INTO Hoteluri(nume,oras,descriere,facilitati,NrStele,pret_pers_noapte,adresa,Telefon,pagina_web) VALUES
('Hotel Ambient',5,'Situat ideal in centrul Brasovului, la cateva minute de monumente istorice, baruri elegante si magazine stralucitoare, acest hotel este un punct de referinta pentru o cazare de lux.',
'Exterior: terasa, terasa la soare
Depozit: schiuri
Mancaruri si bauturi: restaurant, bar
Internet: Gratuit!
Parcare: Gratuita!',4,30,'Bulevardul Iuliu Maniu nr. 30','111111','http://www.hotelambient.ro'),
('Hotel Meteora',5,'Situat la periferia Brasovului.',
'Exterior: terasa
Depozit: schiuri
Mancaruri si bauturi: restaurant, bar, bufet suedez
Internet: Gratuit!
Parcare: Gratuita!',4,25,'Bulevardul Iuliu Groza nr. 21','111551','http://www.hotelmeteora.ro'),
('Hotel Sirena',6,'Situat in centrul Sibiului, la cateva minute de Piata Mare',
'Exterior: terasa, terasa la soare
Mancaruri si bauturi: restaurant, bar
Internet: Gratuit!
Parcare: Gratuita!',4,50,'Bulevardul Piata Mare nr. 30','111112','http://www.hotelpiatamare.ro'),
('Hotel Levski',7,'Situat ideal in centrul Sofiei, cu o priveliste superba',
'Exterior: terasa
Mancaruri si bauturi: restaurant, bar, pub
Internet: Gratuit!
Parcare: Gratuita!',5,75,'Bulevardul Levski nr. 77','222222','http://www.hotellevski.bg'),
('Hotel Bulgaria',7,'Situat mai spre marginea Sofiei, intr-o zona linistita',
'Exterior: terasa
Mancaruri si bauturi: restaurant, bar, pub
Internet: Cu plata!
Parcare: Gratuita!',3,20,'Strada Ruse nr. 71','22342222','http://www.hotelruse.bg'),
('Hotel Grecia',8,'Situat ideal in centrul Atenei, cu o priveliste superba',
'Exterior: terasa, terasa la soare
Mancaruri si bauturi: restaurant, bar
Internet: Gratuit!
Parcare: Gratuita!',4,60,'Bulevardul Acropole nr. 20','29822222','http://www.hotelgrecia.gr'),
('Hotel Atenos',8,'Situat intr-un cartier frumos al Atenei, cu o priveliste superba',
'Exterior: terasa
Mancaruri si bauturi: restaurant
Internet: Cu plata!
Parcare: Gratuita!',3,30,'Bulevardul Acropolis nr. 29','29822111','http://www.hotelacropolis.gr'),
('Hotel Cretos',9,'Situat ideal in centrul insulei Creta, cu o priveliste superba',
'Exterior: terasa interioara, terasa la soare, terasa la umbra
Mancaruri si bauturi: restaurant, bar, pub
Internet: Gratuit!
Parcare: Gratuita!',5,100,'Strada Cretca nr. 12','29822295','http://www.hotelcretos.gr'),
('Hotel CretaIsland',9,'Situat in vestul insulei Creta',
'Exterior: terasa interioara, terasa la soare, terasa la umbra
Mancaruri si bauturi: restaurant, bar
Internet: Cu plata!
Parcare: Gratuita!',3,45,'Strada Palmierilor nr. 10','12822295','http://www.hotelcretaisland.gr'),
('Hotel Pesta',10,'Situat ideal in centrul capitalei Ungariei, cu o priveliste superba',
'Exterior: terasa
Mancaruri si bauturi: restaurant, bar, pub
Internet: Gratuit!
Parcare: Gratuita!',5,90,'Strada Tibor nr. 2','29552295','http://www.hotelpesta.hu'),
('Hotel Buda',10,'Situat spre estul capitalei Ungariei.',
'Exterior: terasa
Mancaruri si bauturi: restaurant, pub
Internet: Gratuit!
Parcare: Gratuita!',4,85,'Strada Kossuth nr. 1','69562295','http://www.hotelbuda.hu'),
('Hotel Wien',11,'Situat ideal in centrul orasului Viena, cu o priveliste superba',
'Exterior: terasa, terasa la soare
Mancaruri si bauturi: restaurant, bar, pub
Internet: Gratuit!
Parcare: Gratuita!',6,150,'Strada Autoband nr. 8','38552288','http://www.hotelwien.au'),
('Hotel Habsburgic',11,'Situat in nordul orasului Viena.',
'Exterior: terasa, terasa la soare
Mancaruri si bauturi: restaurant, bar
Internet: Cu plata!
Parcare: Gratuita!',3,70,'Strada Audi nr. 18','38712288','http://www.hotelhabsburgic.au'),
('Hotel Bruck',12,'Situat ideal in centrul orasului Innsbruck, cu o priveliste superba',
'Exterior: terasa
Depozit: schi
Mancaruri si bauturi: restaurant, bar, pub
Internet: Gratuit!
Parcare: Gratuita!',5,100,'Strada Bruck nr. 9','38552277','http://www.hotelbruck.au'),
('Hotel Inn',12,'Situat in sudul orasului Innsbruck.',
'Exterior: terasa
Depozit: schi
Mancaruri si bauturi: restaurant, bar
Internet: Gratuit!
Parcare: Gratuita!',4,90,'Strada Inn nr. 14','12552277','http://www.hotelinn.au'),
('Hotel Medieval',13,'Situat ideal in centrul orasului Roma, cu o priveliste superba',
'Exterior: terasa
Mancaruri si bauturi: restaurant, bar, pub
Internet: Gratuit!
Parcare: Gratuita!',5,105,'Strada Coloseum nr. 16','38552299','http://www.hotelmedieval.it'),
('Hotel Cezar',13,'Situat in nord-vestul Romei.',
'Exterior: terasa
Mancaruri si bauturi: restaurant, bar, pub
Internet: Cu plata!
Parcare: Gratuita!',4,94,'Strada Cezar nr. 20','15552299','http://www.hotelcezar.it'),
('Hotel Alianz',14,'Situat ideal in centrul orasului Berlin, cu o priveliste superba',
'Exterior: terasa
Mancaruri si bauturi: restaurant, bar, pub
Internet: Gratuit!
Parcare: Gratuita!',5,100,'Strada Munich nr. 33','12352299','http://www.hotelalianz.de'),
('Hotel Mercedes',14,'Situat in sud-vestul orasului Berlin.',
'Exterior: terasa
Mancaruri si bauturi: restaurant, bar
Internet: Gratuit!
Parcare: Gratuita!',4,80,'Strada Koln nr. 32','12342299','http://www.hotelmercedes.de'),
('Hotel Napoleon',15,'Situat ideal in centrul orasului Paris, cu o priveliste superba asupra Turnului Eiffel',
'Exterior: terasa
Mancaruri si bauturi: restaurant, bar, pub
Internet: Gratuit!
Parcare: Gratuita!',6,150,'Strada Sena nr. 1','12352277','http://www.hotelnapoleon.fr'),
('Hotel Ludovic',15,'Situat ideal in estul orasului Paris.',
'Exterior: terasa
Mancaruri si bauturi: restaurant, bar
Internet: Gratuit!
Parcare: Gratuita!',4,89,'Strada Luvru nr. 3','12353377','http://www.hotelludovic.fr'),
('Hotel Cazino',16,'Situat ideal in centrul orasului Monte-Carlo, cu o priveliste superba asupra Coastei de Azur',
'Exterior: terasa
Mancaruri si bauturi: restaurant, bar, pub
Internet: Gratuit!
Parcare: Gratuita!',6,300,'Strada Mediterana nr. 10','12352299','http://www.hotelcazino.fr'),
('Hotel Excelent',16,'Situat in nordul orasului Monte-Carlo, cu o priveliste superba asupra Coastei de Azur',
'Exterior: terasa
Mancaruri si bauturi: restaurant, bar, pub
Internet: Gratuit!
Parcare: Gratuita!',5,200,'Strada Country nr. 17','12352678','http://www.hotelexcelent.fr'),
('Hotel Londoner',17,'Situat ideal in centrul orasului Londra, cu o priveliste superba asupra Big Ben-ului',
'Exterior: terasa
Mancaruri si bauturi: restaurant, bar, pub
Internet: Gratuit!
Parcare: Gratuita!',5,250,'Strada Westminster nr. 40','98352299','http://www.hotellondoner.uk'),
('Hotel United',17,'Situat in nordul orasului Londra',
'Exterior: terasa
Mancaruri si bauturi: restaurant, bar
Internet: Cu plata!
Parcare: Gratuita!',3,100,'Strada Whitechapel nr. 39','98351699','http://www.hotelwhitechapel.uk'),
('Hotel Nadal',18,'Situat ideal in centrul orasului Madrid.',
'Exterior: terasa
Mancaruri si bauturi: restaurant, bar, pub
Internet: Gratuit!
Parcare: Gratuita!',5,220,'Strada Los Desperados nr. 40','98542299','http://www.hotelnadal.sp'),
('Hotel Verdasco',18,'Situat in nord-estul orasului Madrid.',
'Exterior: terasa
Mancaruri si bauturi: restaurant, bar
Internet: Gratuit!
Parcare: Gratuita!',4,120,'Strada Los Sepulcros nr. 31','98542123','http://www.hotelverdasco.sp');

-- ----------------------------------------------------------------------------------------------------------

### Inserari in tabela Departament ###

INSERT INTO Departament(denumire) VALUES 
('conducere'),
('administratie'),
('transport'),
('marketing'),
('intretinere'),
('vanzari');

-- -----------------------------------------------------------------------------------------------------------

### Inserari in tabela Sediu ###

INSERT INTO Sediu(oras,denumire,adresa,telefon) VALUES (1,'GicaWorldTour Cluj-Napoca','Str. Campului, Nr. 2', '0756472082'),
(2,'GicaWorldTour Bucuresti','Str. romana, Nr. 21','0750234567'),
(3,'GicaWorldTour Iasi','Bulevardul 1 Decembrie, Nr. 28','0790765432'),
(4,'GicaWorldTour Timisoara','Strada Resitei, Nr. 5,','0720956789');

-- ------------------------------------------------------------------------------------------------------------

### Inserari in tabela Personal ###

INSERT INTO personal(nume,prenume,adresa,telefon,functie,salariu,data_angajarii,id_sediu,id_departament)
VALUES ('Blidar','George','Mun. Cluj-Napoca, Str. Ion Ghica, Nr. 3A','0756472082','Director General',10000,'2001/07/20',1,1),

('Giurgi','Iacob','Mun. Cluj-Napoca, Str. Avram Iancu, Nr. 28','0778909899','Director Departament Cluj-Napoca',7500,'2001/08/16',1,1),

('Popa','Bogdan','Mun. Timisoara, Str. Ion Agrijan, Nr. 35','0723257789','Director Departament Timisoara',7500,'2001/09/10',4,1),

('Grec','Mihai','Mun. Bucuresti, Str. Mihai Eminescu, Nr. 7','0725502123','Director Departament Bucuresti',7500,'2001/10/13',2,1),

('Anca','Mihai','Mun. Iasi, Splaiul Independentei, Nr. 23','0752957555','Director Departament Iasi',6500,'2004/03/29',3,1),

('Negrean','Victor','Mun. Cluj-Napoca, Str. Observatorului, Nr. 22','0735195167','Administrator web',4000,'2002/01/29',1,2),

('Bagarean','George','Mun. Timisoara, Str. Rozelor, Nr. 2','0746133545','Administrator web',4000,'2002/01/15',4,2),

('Fabian','Andreea','Mun. Bucuresti, Str. Oasului, Nr. 55','0721647284','Administrator web',4000,'2002/02/21',2,2),

('Obreja','Vlad','Mun. Iasi, Str. Moldovitei, Nr. 10','0744817666','Administrator web',4000,'2004/03/11',3,2),

('Baidoc','Cristian','Mun. Cluj-Napoca, Str. Sanatorului, Nr. 8','0752913569','Administrator garaj',2300,'2002/02/19',1,2),

('Cuceu','Bogdan','Mun. Timisoara, Str. Porumbeilor, Nr. 5','0724191377','Administrator garaj',2200,'2002/05/17',4,2),

('Carp','Sergiu','Mun. Bucuresti, Bulevardul Garii, Nr. 22','0728501849','Administrator garaj',2350,'2002/05/13',2,2),

('Palade','Tudor','Mun. Iasi, Str. Iacobeni, Nr. 8','0773918264','Administrator garaj',2140,'2004/04/22',3,2),

('Vescan','Catalin','Mun. Cluj-Napoca, Str. Mestecanis, Nr. 52','0752198544','Jurist',2600,'2001/11/30',1,2),

('Vranau','Flavius','Mun. Cluj-Napoca, Str. Morarilor, Nr. 32','0726916544','Contabil',2450,'2001/12/12',1,2),

('Lupu','Cristina','Mun. Timisoara, Str. Oltului, Nr. 27','0724916546','Jurist',2520,'2001/11/15',4,2),

('Jurcau','Maria','Mun. Timisoara, Str. Muresului, Nr. 28','0744159511','Contabil',2360,'2002/02/04',4,2),

('Mates','Brigitta','Mun. Bucuresti, Str. Constantin Brancusi, Nr. 49','0742627403','Jurist',2700,'2001/12/16',2,2),

('Pop','Cristian','Mun. Bucuresti, Bulevardul Closca, Nr. 20','0741845033','Contabil',2520,'2002/03/16',2,2),

('Gurzau','Ionut','Mun. Iasi, Bulevardul Unirea, Nr. 32','0736910222','Jurist',2490,'2004/04/28',3,2),

('Visovan','Francisc','Mun Iasi, Str. Iuliu Maniu, Nr. 7','0751280212','Contabil',2400,'2004/06/01',3,2),

('Vlaicu','Paul','Mun. Cluj-Napoca, Str. Simion Barnutiu, Nr. 40','0741200326','Sofer',1500,'2001/11/09',1,3),

('Dragos','Bogdan','Mun. Cluj-Napoca, Str. Primaverii, Nr. 76','0742389151','Sofer',1650,'2001/11/05',1,3),

('Dumuta','Darius','Mun. Cluj-Napoca, Calea Motilor, Nr 19','0752439810','Sofer',1500,'2001/11/01',1,3),

('Budai','Vladut','Mun. Cluj-Napoca,Calea Floresti, Nr 39','0724317111','Sofer',1500,'2002/09/17',1,3),

('Nechifor','Cosmin','Mun. Bucuresti, Str. Marinei, Nr. 6','0758161255','Sofer',1460,'2008/04/25',2,3),

('Curtean','Alex','Mun. Bucuresti, Str. Minerului, Nr. 2','0722820991','Sofer',1600,'2005/01/30',2,3),

('Ardelean','Ionas','Mun. Bucuresti, Str. Nicolae Titulescu, Nr. 90','0751382476','Sofer',1600,'2004/06/21',2,3),

('Rus','Mihai','Mun. Bucuresti, Str. Hortensiei, Nr. 84','0728741320','Sofer',1600,'2007/11/08',2,3),

('Eminovici','Paul','Mun. Iasi, Str. Berzei, Nr. 109','0753856195','Sofer',1300,'2009/07/14',3,3),

('Ciupe','Danut','Mun. Iasi, Str. Pitesti, Nr. 34','0751346729','Sofer',1400,'2008/02/27',3,3),

('Morar','Florin','Mun. Iasi, Str. Vasile Alecsandri, Nr. 143','0729375976','Sofer',1300,'2011/09/15',3,3),

('Bulgarean','Vasile','Mun. Iasi, Aleea Pictorilor, Nr. 2','0744518220','Sofer',1400,'2006/03/14',3,3),

('Mandache','Andrei','Mun. Timisoara, Str. Lapusului, Nr. 12','0741248851','Sofer',1350,'2010/11/08',4,3),

('Dragomir','Mitica','Mun. Timisoara, Bulevardul 16 Decembrie 1989, Nr. 38','0728946185','Sofer',1350,'2009/07/16',4,3),

('Mircea','Sandu','Mun. Timisoara, Calea Aradului, Nr. 82','0742597670','Sofer',1450,'2007/01/22',4,3),

('Badea','Mircea','Mun. Timisoara, Str. Vasile Lucaciu, Nr. 2A','0728516844','Sofer',1350,'2006/11/06',4,3),

('Dragos','Grigore','Mun. Cluj-Napoca, Calea Turzii, Nr. 15','0724826591','Ghid',1800,'2005/10/12',1,3),

('Danciu','Cristian','Mun. Cluj-Napoca, Str. Morii, Nr. 44','0745581029','Ghid',1800,'2009/06/01',1,3),

('Nechita','Ioana','Mun. Bucuresti, Str. Constanta, Nr. 33','0744383529','Ghid',1850,'2006/08/30',2,3),

('Protopopu','Patriciu','Mun. Bucuresti, Bulevardul Miron Costin, Nr. 71','0745742894','Ghid',1850,'2009/03/01',2,3),

('Cioban','Ovidiu','Mun. Iasi, Calea Victoriei, Nr. 63','0744859221','Ghid',1700,'2009/06/17',3,3),

('Suciu','Rares','Mun. Iasi, Aleea Neptun, Nr. 11','0747339580','Ghid',1700,'2013/08/02',3,3),

('Rosca','Dana','Mun. Timisoara, Bulevardul Eroilor, Nr. 158','0744084312','Ghid',1750,'2005/04/30',4,3),

('Danci','Oana','Mun. Timisoara, Str. Bisericii, Nr. 5','0756397951','Ghid',1750,'2010/03/21',4,3),

('Hanca','Sergiu','Mun. Cluj-Napoca, Str. Louis Pasteur, Nr. 23A','0745361956','Manager Marketing',2300,'2002/10/05',1,4),

('Niculescu','Monica','Mun. Bucuresti, Aleea Caisilor, Nr. 51','0744239006','Manager Marketing',2400,'2002/07/26',2,4),

('Ciaca','Rares','Mun. Iasi, Str. Transilvania, Nr. 9','0729368244','Manager Marketing',2250,'2005/02/15',3,4),

('Mastan','Alexandru','Mun. Timisoara, Str. Olarilor, Nr. 58','0742361276','Manager Marketing',2250,'2003/05/20',4,4),

('Berceanu','Marcel','Mun. Cluj-Napoca, Str. Iazului, Nr. 4','0728455945','Mecanic auto',1550,'2002/04/14',1,5),

('Revnic','Gheorghe','Mun. Bucuresti, Str. Mihai Viteazu, Nr. 19','0744395701','Mecanic auto',1400,'2013/10/04',2,5),

('Salajan','Razvan','Mun. Iasi, Str. Campului, Nr.46','0742947617','Mecanic auto',1500,'2007/07/25',3,5),

('Puscas','Sergiu','Mun. Timisoara, Str. Ciocarliei, Nr. 68','0748395290','Mecanic auto',1550,'2003/12/12',4,5),

('Coste','Iustin','Mun. Cluj-Napoca, Str. Regele Ferdinand, Nr. 14','0724581652','Vanzator',1250,'2003/11/14',1,6),

('Todoran','Adrian','Mun. Cluj-Napoca, Str. Potcoavei, Nr. 6','0742359700','Vanzator',1100,'2012/04/23',1,6),

('Nedelcearu','Oliviu','Mun. Bucuresti, Str. Mitropoliei, Nr. 3','0723491742','Vanzator',1350,'2004/10/02',2,6),

('Marton','Atilla','Mun. Bucuresti, Bulevardul Ghencea, Nr. 341','0729374608','Vanzator',1200,'2010/03/18',2,6),

('Hamza','Ramona','Mun. Iasi, Str. Trandafirului, Nr. 44','0746185589','Vanzator',1150,'2004/10/01',3,6),

('Burleanu','Razvan','Mun. Iasi, Str. Spaluil Republicii, Nr. 16','0756938813','Vanzator',1150,'2008/02/25',3,6),

('Banciu','Radu','Mun. Timisoara, Str. Craiova, Nr. 11','0744715406','Vanzator',1250,'2006/01/16',4,6),

('Hotea','Sebastian','Mun. Timisoara, Str. Scolii, Nr. 17B','0725759292','Vanzator',1100,'2013/05/18',4,6);

-- ---------------------------------------------------------------------------------------------------------------

### Inserari in tabela Autocare ###

INSERT INTO autocare(marca, model, data_achizitiei,numar_locuri,descriere_tehnica,id_sediu) VALUES
('Mercedes','Busses','2001/11/10',70,
'Cutie Automata: Da
 Aer Conditionat: Da
 Wifi: Nu
 Televizor: Da',1),
('Irisbus','Busses','2004/09/12',80,
'Cutie Automata: Da
 Aer Conditionat: Da
 Wifi: Nu
 Televizor: Da',1),
('Mercedes','Busses','2002/11/10',72,
'Cutie Automata: Nu
 Aer Conditionat: Da
 Wifi: Da
 Televizor: Nu',1),
 ('Setra','Busses','2000/11/12',65,
'Cutie Automata: Da
 Aer Conditionat: Da
 Wifi: Da
 Televizor: Da',2),
 ('Iveco','Busses','2006/09/12',70,
'Cutie Automata: Nu
 Aer Conditionat: Da
 Wifi: Nu
 Televizor: Nu',2),
('Setra','Busses','2005/11/01',68,
'Cutie Automata: Da
 Aer Conditionat: Da
 Wifi: Nu
 Televizor: Nu',2),
('Iveco','Busses','2001/10/12',80,
'Cutie Automata: Nu
 Aer Conditionat: Da
 Wifi: Nu
 Televizor: Da',3),
 ('Mercedes','Busses','2005/01/12',50,
'Cutie Automata: Da
 Aer Conditionat: Da
 Wifi: Nu
 Televizor: Da',3),
('Iveco','Busses','2006/12/12',74,
'Cutie Automata: Da
 Aer Conditionat: Da
 Wifi: Da
 Televizor: Da',3),
('Setra','Busses','2001/09/11',81,
'Cutie Automata: Da
 Aer Conditionat: Da
 Wifi: Da
 Televizor: Nu',4),
('Irisbus','Busses','2006/03/12',81,
'Cutie Automata: Da
 Aer Conditionat: Da
 Wifi: Nu
 Televizor: Da',4),
 ('Irisbus','Busses','2005/07/11',65,
'Cutie Automata: Nu
 Aer Conditionat: Da
 Wifi: Da
 Televizor: Nu',4);
 
-- -----------------------------------------------------------------------------------------------
 
 ### Inserari in tabela Concedii ###
 
INSERT INTO Concedii(id_personal,data_start,data_sfarsit) VALUES 
(1,'2016/07/01','2016/07/31'),
(1,'2017/08/01','2017/08/31'),
(2,'2016/08/01','2016/08/31'),
(2,'2017/07/01','2017/07/31'),
(3,'2016/06/01','2016/06/30'),
(3,'2017/07/15','2017/08/14'),
(4,'2016/07/15','2016/08/14'),
(4,'2017/06/15','2017/07/14'),
(5,'2016/07/20','2016/08/19'),
(5,'2017/06/20','2017/07/19'),
(6,'2016/01/01','2016/01/31'),
(6,'2017/06/01','2017/06/30'),
(7,'2016/02/01','2016/03/02'),
(7,'2017/07/01','2017/07/31'),
(8,'2016/03/01','2016/03/31'),
(8,'2017/08/01','2017/08/31'),
(9,'2016/04/01','2016/04/30'),
(9,'2017/08/15','2017/09/14'),
(10,'2016/05/01','2016/05/30'),
(10,'2017/08/10','2017/09/09'),
(11,'2016/06/15','2016/07/14'),
(11,'2017/09/01','2017/09/30'),
(12,'2016/07/01','2016/07/31'),
(12,'2017/10/01','2017/10/31'),
(13,'2016/07/15','2016/08/14'),
(13,'2017/10/10','2017/11/09'),
(14,'2016/08/01','2016/08/31'),
(14,'2017/11/01','2105/11/30'),
(15,'2016/08/15','2016/09/14'),
(15,'2017/11/15','2017/12/14'),
(16,'2016/01/15','2016/02/14'),
(16,'2017/06/15','2017/07/14'),
(17,'2016/02/15','2016/03/16'),
(17,'2017/07/15','2017/08/14'),
(18,'2016/03/15','2016/04/14'),
(18,'2017/08/15','2017/09/14'),
(19,'2016/04/15','2016/05/14'),
(19,'2017/09/15','2017/10/14'),
(20,'2016/05/15','2016/06/14'),
(20,'2017/10/15','2017/11/14'),
(21,'2016/06/15','2016/07/14'),
(21,'2017/11/15','2017/12/14'),
(22,'2017/12/01','2017/12/31'),
(23,'2017/12/01','2017/12/31'),
(24,'2017/12/01','2017/12/31'),
(25,'2017/12/01','2017/12/31'),
(26,'2017/12/01','2017/12/31'),
(27,'2017/12/01','2017/12/31'),
(28,'2017/12/01','2017/12/31'),
(29,'2017/12/01','2017/12/31'),
(30,'2017/12/01','2017/12/31'),
(31,'2017/12/01','2017/12/31'),
(32,'2017/12/01','2017/12/31'),
(33,'2017/12/01','2017/12/31'),
(34,'2017/12/01','2017/12/31'),
(35,'2017/12/01','2017/12/31'),
(36,'2017/12/01','2017/12/31'),
(37,'2017/12/01','2017/12/31'),
(38,'2017/12/01','2017/12/31'),
(39,'2017/12/01','2017/12/31'),
(40,'2017/12/01','2017/12/31'),
(41,'2017/12/01','2017/12/31'),
(42,'2017/12/01','2017/12/31'),
(43,'2017/12/01','2017/12/31'),
(44,'2017/12/01','2017/12/31'),
(45,'2017/12/01','2017/12/31'),
(46,'2016/05/01','2016/05/31'),
(46,'2017/12/01','2017/12/31'),
(47,'2016/07/15','2016/08/14'),
(47,'2017/01/15','2017/02/14'),
(48,'2016/08/15','2016/09/14'),
(48,'2017/02/15','2017/03/16'),
(49,'2016/09/15','2016/10/14'),
(49,'2017/03/15','2017/04/14'),
(50,'2016/10/15','2016/11/14'),
(50,'2017/04/15','2017/05/14'),
(51,'2016/11/15','2016/12/14'),
(51,'2017/05/15','2017/06/14'),
(52,'2016/01/01','2016/01/31'),
(52,'2017/08/01','2017/08/31'),
(53,'2016/02/01','2016/03/02'),
(53,'2017/09/01','2017/09/30'),
(54,'2016/03/01','2016/03/31'),
(54,'2017/10/01','2017/10/30'),
(55,'2016/04/01','2016/04/30'),
(55,'2017/11/01','2017/11/30'),
(56,'2016/05/01','2016/05/31'),
(56,'2017/12/01','2017/12/31'),
(57,'2016/07/15','2016/08/14'),
(57,'2017/01/15','2017/02/14'),
(58,'2016/08/15','2016/09/14'),
(58,'2017/02/15','2017/03/16'),
(59,'2016/09/15','2016/10/14'),
(59,'2017/03/15','2017/04/14'),
(60,'2016/10/15','2016/11/14'),
(60,'2017/04/15','2017/05/14'),
(61,'2016/11/15','2016/12/14'),
(61,'2017/05/15','2017/06/14');
-- -------------------------------------------------------------------------------------------------------

### Inserari in tabela Utilizatori ###

INSERT INTO Utilizatori(username,parola,email,nume,prenume,adresa,telefon,sex,admin) VALUES
('BlidarDenisa','blidardenisa','blidardenisa@yahoo.com','Blidar','Denisa','Zalau, Salaj','0725481544','F','0'),

('CiacaRares','ciacarares','ciacarares@yahoo.com','Ciaca','Rares','Timisoara, Timis','0756472000','M','0'),

('IonescuLucian','ionesculucian','IonescuLucian@yahoo.com','Ionescu','Lucian','Zalau, Salaj','0775177633','M','0'),

('FerencTomus','ferenctomus','FerencTomus@yahoo.com','Tomus','Ferenc','Cluj-Napoca, Cluj','0751746123','M','0'),

('MoldovanRaluca','moldovanraluca','MoldovanRaluca@yahoo.com','Moldovan','Radluca','Oradea, Bihor','0744856111','F','0'),

('ProtopopuGabriel','protopopugabriel','protopopugabriel@yahoo.com','Protopopu','Gabriel','Satu-Mare, Satu-Mare','0725481599','M','0'),

('NechitaIoana','nechitaioana','nechitaioana@yahoo.com','Nechita','Ioana','Falticeni, Suceava','0756472111','F','0'),

('SestrasFlorica','sestrasflorica','sestrasflorica@yahoo.com','Sestras','Florica','Bucuresti, Ilfov','0735177622','F','0'),

('AntonescuCrin','antonescucrin','antonescucrin@yahoo.com','Antonescu','Crin','Constanta, Constanta','0751746999','M','0'),

('BurgheleAndrei','burgheleandrei','burgheleandrei@yahoo.com','Burghele','Andrei','Iasi, Iasi','0744789123','M','0'),

('gica96','gblidar','blidar_george@yahoo.com','Blidar','George','Cluj-Napoca, Cluj','0756472082','M','1');

