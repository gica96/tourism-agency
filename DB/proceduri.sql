## Aceasta procedura are rolul de a inregistra o cumparare ( vanzare ) a unui sejur => Se precizeaza utilizatorul
## care a cumparat sejurul, id-ul sejurului, calcularea pretului sejurului in functie de numarul de oferte achizitionate
## si inregistrarea rezervarilor la unul din hotelurile aflate in orasele destinatie
## Mai intai, verificam daca oferta mai este inca valabila, adica daca data de inceput a sejurului este mai mare
## decat data curenta
## Urmatoarea verificare se face asupra numarului de locuri disponibile intr-un sejur
## Pe urma, se vor introduce rezervarile in tabela rezervare_hotel, cu niste precizari: Folosim o bucla pentru
## a realiza acest lucru. Verificam cu ajutorul functiei split_string daca hotelul este in orasul destinatie. Pe urma,
## verificam data la care incepe cazarea la respectivul hotel, care este egala cu data de sfarsit al traseului
## ce are drepet oras destinatie orasul in care se afla actualul hotel. Analog se procedeaza pentru data de sfarsit
## al cazarii, care are este egala cu data de inceput al urmatorului traseu ce are drept oras de plecare orasul curent
## in care se afla hotelul actual. Pe urma, toate aceste inregistrari sunt trecute in tabela rezervare_hotel
## Se modifica si pretul total de persoana, adaugand la acesta si pretul cazarii
## Se iese pe urma din bucla si se calculeaza pretul transportului. Acesta se adauga pretului total de persoana
## Pe urma, in variabile "pret" se calculeaza pretul total pentru numarul de sejururi cumparate, adica 
## pretul total de persoana calculat mai sus * numarul de sejururi cumparate
## Apoi, se calcleaza reducerea de care va beneficia un client. Aceasta este = cu numarul de puncte de fidelitate
## Daca numarul de puncte >= decat pretul total, atunci pretul sejurului va deveni 0
## Altfel, se modifica numarul de puncte de fidelitate ale clientului, astfel incat acestea sa reprezinte 10% din pretul
## final al sejurului
## In final, se afiseaza un mesaj de succes.


drop procedure if exists Cumparare_Sejur;
delimiter //

create procedure Cumparare_Sejur(_ID_Utilizator int, _ID_Sejur int, _NrSejururi int, string_hoteluri varchar(300))

  begin
	
	declare locuri integer;
    declare durata_rezervare integer;
    declare data_start_rezervare,data_sfarsit_rezervare datetime;
	declare i,puncte_fidelitate_user,pret integer default 0;
	declare nr_hoteluri integer;
    set nr_hoteluri=(select count(id_sejur) from traseu where id_sejur=_id_sejur);
    set nr_hoteluri=nr_hoteluri-1;
	set @data_start = (select data_start from sejur where id_sejur=_id_sejur);
    set @dif = timestampdiff(day,current_date(), @data_start);
    
    if (@dif < 0) then select "Oferta expirata! " as Mesaj;
    else
		set locuri = (select nr_locuri from sejur where id_sejur=_id_sejur);
		if(locuri>=_nrsejururi) then
			insert into contractare_sejur(id_utilizator,id_Sejur,nrsejururi) values
            (_id_utilizator,_id_sejur,_nrsejururi);    
           set @contract= (select max(id_contract) from contractare_sejur);
           
           
            loop_insert: loop
			set i=i+1;
            if i=nr_hoteluri+1 then
				leave loop_insert;
			end if;
				
                set @hotel = SPLIT_STRING(string_hoteluri,',',i) ;
				
                    
                    #verific daca @hotel e in orasu corespunzator
			
            set @verifica_hotel=null;
            
            set @verifica_hotel=(select id_hotel from traseu,hoteluri where traseu.oras2=hoteluri.oras and traseu.id_sejur=_id_sejur and id_hotel=@hotel);
            
            if (@verifica_hotel is null) then
            select "Hotelul nu este din orasul destinatie" as Mesaj;
            leave loop_insert;
            else
            
			set data_start_rezervare = 
            (select traseu.data_sfarsit
            from traseu,hoteluri
            where traseu.id_sejur=_id_sejur
            and hoteluri.id_hotel=@hotel
            and traseu.oras2=hoteluri.oras
            group by traseu.data_start);                  
                    
				
			set data_sfarsit_rezervare = 
			(select traseu.data_start
            from traseu,hoteluri
            where traseu.id_sejur=_id_sejur
            and hoteluri.id_hotel=@hotel
            and traseu.oras1=hoteluri.oras
            group by traseu.data_start);
                   
                    
			set @zi_start = dayofyear(data_start_rezervare);
			set @zi_sfarsit = dayofyear(data_sfarsit_rezervare);
			            
            set durata_rezervare = @zi_sfarsit - @zi_start;
            if (durata_rezervare < 0 ) then
            #inseamna ca prima data este in alt an fata de a doua
				set durata_rezervare = durata_rezervare + 365;
			end if;
			
			
            
            set @pret_rezervare = (select pret_pers_noapte from hoteluri where id_hotel=@hotel);
 
			if(durata_rezervare<=1) then
		set @pret = @pret_rezervare;
        else
		set @pret = @pret_rezervare * durata_rezervare;
			end if;

                    
                    
			insert into rezervare_hotel(id_contract,ID_Hotel,data_start,data_Sfarsit,pret,durata) 
            values (@contract,@hotel,data_start_rezervare,data_sfarsit_rezervare,@pret,durata_rezervare);
			update contractare_sejur set pret_pers=pret_pers+@pret where id_contract=@contract;
                    
        end if;
        end loop;
		if(@verifica_hotel is not null) then
		set @pret_transport=(select pret_transport from sejur where id_sejur=_id_sejur);
		update contractare_sejur set pret_pers=pret_pers+@pret_transport where id_contract=@contract;
        set pret=(select pret_pers from contractare_sejur where id_contract=@contract)*_nrsejururi;
		set puncte_fidelitate_user =(select puncte_fidelitate from utilizatori where id_utilizator=_id_utilizator);
        if(puncte_fidelitate_user>=pret) then
        update contractare_Sejur set reduceri=pret where id_contract=@contract;
        update contractare_Sejur set pret_final=0 where id_contract=@contract;
        update utilizatori set puncte_fidelitate = puncte_fidelitate_user - pret where ID_Utilizator=_ID_Utilizator;
        else
        update contractare_Sejur set reduceri=puncte_fidelitate_user where id_contract=@contract;
        update contractare_Sejur set pret_final=pret-puncte_fidelitate_user where id_contract=@contract;
        
        update utilizatori set puncte_fidelitate = (pret-puncte_fidelitate_user) / 10 where ID_Utilizator=_ID_Utilizator;
        
           end if;
			
        select "Succes" as Mesaj;
        else delete from contractare_sejur where id_contract=@contract;
        end if;
		else select "Nu mai exista locuri" as Mesaj;
        end if;
	end if;
    
    
  end //
delimiter ;


## Procedura "adaugare_sejur": Are rolul de a introduce noi sejururi in baza de date
## Se incepe verificarea disponibilitatii autocarului, soferilor si a ghidului in perioada specificata, adica daca acestia
## nu sunt implicati intr-un alt sejur sau sunt in concediu
## Daca datele sunt valide, se introduc in tabela sejur
## Mai departe, procedura adauga in tabela traseu, datele corespunzatoare. Cu ajutorul buclei, verificam daca am
## atins numarul de trasee dorite pentru acel sejur. Cu ajutorul functiei create "split_string", care prelucreaza
## un anumit sir introdus ca parametru, se verifica datele ce urmeaza a fi introduse in tabela traseu
## Conditia necesara si suficenta este ca data de inceput sa fie mai mica ca si data de sfarsit, altfel se va afisa
## un mesaj de eroare
## In final, se updateaza numarul de locuri al sejurului, cu numarul de pasageri al autocarului care va realiza sejurul.


drop procedure if exists Adaugare_sejur;
delimiter //

create procedure Adaugare_Sejur(_id_autocar int, _id_sofer1 int, _id_sofer2 int,_id_ghid int, nr_trasee int, string_orase1 varchar(300), string_orase2 varchar(300),string_data_start varchar(300),string_data_sfarsit varchar(300),string_distante varchar(300))
  begin
			declare i integer default 0;
        
        
		set @verificare_autocar = null;
        set @verificare_sofer1 = null;
		set @verificare_sofer2 = null;
		set @verificare_ghid = null;
		set @verificare_concediu1 = null;
		set @verificare_concediu2 = null;
		set @verificare_concediu3 = null;
		set @data_start = SPLIT_STRING(string_data_start,',',1) ;
		set @data_sfarsit = SPLIT_STRING(string_data_sfarsit,',',nr_trasee);
       
        set @verificare_autocar=(select id_autocar from sejur where id_autocar=_id_autocar and not (@data_start > data_sfarsit or @data_sfarsit < data_start));
        set @verificare_sofer1= (select sofer1 from sejur where sofer1=_id_sofer1 and not (@data_start > data_sfarsit or @data_sfarsit < data_start));
        set @verificare_sofer2=(select sofer2 from sejur where sofer2=_id_sofer2 and not (@data_start > data_sfarsit or @data_sfarsit < data_start));
		set @verificare_sofer1=(select sofer1 from sejur where sofer2=_id_sofer1 and not (@data_start > data_sfarsit or @data_sfarsit < data_start));
        set @verificare_sofer2=(select sofer2 from sejur where sofer1=_id_sofer2 and not (@data_start > data_sfarsit or @data_sfarsit < data_start));
        set @verificare_ghid=(select ghid from sejur where ghid=_id_ghid and not (@data_start > data_sfarsit or @data_sfarsit < data_start));
        
        #verific daca soferii nu sunt in concediu
        set @verificare_concediu1=(select id_personal from concedii where id_personal=_id_sofer1 and not(@data_start > data_sfarsit or @data_sfarsit < data_start));
        set @verificare_concediu2=(select id_personal from concedii where id_personal=_id_sofer2 and not(@data_start > data_sfarsit or @data_sfarsit < data_start));
        set @verificare_concediu3=(select id_personal from concedii where id_personal=_id_ghid and not(@data_start > data_sfarsit or @data_sfarsit < data_start));
        
        
               
        if (@verificare_autocar is null and @verificare_sofer1 is null and @verificare_sofer2 is null) then
		if (@verificare_concediu1 is null and @verificare_concediu2 is null) then
			insert into sejur (id_autocar,sofer1,sofer2,ghid) values (_id_autocar,_id_sofer1,_id_sofer2,_id_ghid);
			set @id_sejur = (select max(id_sejur) from sejur); 
			     
        loop_insert: loop
			set i=i+1;
            if i=nr_trasee+1 then
				leave loop_insert;
			end if;
				set @oras1 = SPLIT_STRING(string_orase1,',',i) ;
                set @oras2 = SPLIT_STRING(string_orase2,',',i) ;
                set @date1 = SPLIT_STRING(string_data_start,',',i) ;
                set @date2 = SPLIT_STRING(string_data_sfarsit,',',i) ;
                set @distanta = SPLIT_STRING(string_distante,',',i) ;
				
				set @durata = timestampdiff(second, @date1, @date2);
                if(@durata < 0) then 
                delete from sejur where id_sejur=@id_sejur;     
                select "Perioada specificata nu este valida (este descrescatoare)! " as Mesaj;
                leave loop_insert;
				else
                
				insert into traseu(id_sejur,oras1,oras2,data_start,data_sfarsit,`distanta(km)`) values
				(@id_sejur,@oras1,@oras2,@date1,@date2,@distanta);
               
				
               

				end if;
        
        end loop;
        if(@durata > 0) then
        set @locuri=(select numar_locuri from autocare where id_autocar=_id_autocar);
        update sejur set nr_locuri=@locuri where id_sejur=@id_Sejur;
		select "Succes" as Mesaj;
        end if;
        
        else select "Unul dintre soferi sau ghidul este in concediu in perioada specificata" as Mesaj;
        end if;
        
        else select "Autocarul, soferii sau ghidul nu sunt disponibili in perioada specificata! " as Mesaj;
        end if;
        
      
        
  
	
  end //
delimiter ;


## Procedura "update_soferi" : Are rolul de a modifica numarul de curse efectuate de fiecare sofer
## Cu un cursor parcurgem fiecare sofer in parte, iar cu variabilele @sofer1 si @sofer2 stocam nr. de curse efectuate de fiecare sofer in parte, atat de pe pozitia sofer_1 cat si de pe pozitia sofer_2



DROP procedure IF EXISTS update_soferi_procedure;

DELIMITER //

create procedure update_soferi_procedure()
BEGIN
		DECLARE finished INTEGER DEFAULT 0;
		declare soferi integer;
        DEClARE soferi_cursor CURSOR FOR  SELECT ID_Sofer FROM soferi;
		DECLARE CONTINUE HANDLER FOR NOT FOUND SET finished = 1;

	open soferi_cursor;
	soferi_loop: loop
		fetch soferi_cursor into soferi;
    
		IF finished = 1 THEN 
		LEAVE soferi_loop;
		END IF;
        set @sof1=(select count(sofer1) from sejur where sofer1=soferi and data_sfarsit<current_date);
        set @sof2=(select count(sofer2) from sejur where sofer2=soferi and data_sfarsit<current_date);
        update soferi set nrcurse=@sof1+@sof2 where id_sofer=soferi;
        
        end loop soferi_loop;


END //

DELIMITER ;

## Procedura "update_autocare" : are rolul de a modifica numarul de kilometri parcursi de fiecare autocar
## Cu un index parcuregm toate autocarele inregistrate, iar daca sejurul s-a terminat, modificam nr de km. parcrsi de 
## autocar

DROP procedure IF EXISTS update_autocare_procedure;

DELIMITER //

create procedure update_autocare_procedure()
BEGIN
		DECLARE finished INTEGER DEFAULT 0;
		declare autocare integer;
        DEClARE autocare_cursor CURSOR FOR  SELECT ID_Autocar FROM autocare;
		DECLARE CONTINUE HANDLER FOR NOT FOUND SET finished = 1;
  		
	open autocare_cursor;
	autocare_loop: loop
		fetch autocare_cursor into autocare;
    
		IF finished = 1 THEN 
		LEAVE autocare_loop;
		END IF;
		set @km=(select sum(distanta_totala) from sejur where id_autocar=autocare and data_sfarsit<current_date);
       if(@km is null) then set @km=0;
       end if;
		update autocare set km_parcursi=@km where id_autocar=autocare;
        end loop autocare_loop;


END //

DELIMITER ;