## 1) Denumirea oraselor de tip montan din Romania

SELECT 
    orase.denumire
FROM
    tip_oras
        JOIN
    orase ON orase.id_tip = tip_oras.id_tip
        AND orase.tara = 'Romania'
        AND tip_oras.tipul = 'montan'; 

## 2) Orasele de tip litoral care nu sunt intr-un sejur activ momentan

SELECT 
    orase.denumire
FROM
    orase
        JOIN
    tip_oras ON tip_oras.id_tip = orase.id_tip
        AND tipul = 'litoral'
        AND id_oras NOT IN (SELECT 
            traseu.oras1
        FROM
            traseu
        WHERE
            traseu.Data_start > CURRENT_DATE
        GROUP BY id_sejur);

## 3) Numele si prenumele utilizatorilor care au cumparat sejururi

SELECT 
    utilizatori.nume, utilizatori.prenume
FROM
    utilizatori
        JOIN
    contractare_sejur ON utilizatori.id_utilizator = contractare_sejur.id_utilizator
GROUP BY utilizatori.id_utilizator;

## 4) Clientii care au cumparat cel putin doua excursii

SELECT 
    Username
FROM
    utilizatori
HAVING (SELECT 
        SUM(contractare_sejur.NrSejururi)
    FROM
        contractare_sejur
    WHERE
        contractare_sejur.id_utilizator = utilizatori.ID_Utilizator
    GROUP BY contractare_sejur.id_utilizator) >= 2;

## 5) Clientii care au cumparat excursii din Cluj

SELECT 
    Username
FROM
    utilizatori
        JOIN
    contractare_sejur ON utilizatori.id_utilizator = contractare_sejur.id_utilizator
        JOIN
    traseu ON contractare_sejur.id_sejur = traseu.id_sejur
        JOIN
    orase ON traseu.oras1 = orase.id_oras
        AND orase.denumire = 'Cluj-Napoca'
GROUP BY utilizatori.id_utilizator;

## 6) Toti clientii care au contractat excursii din cel putin 2 orase diferite de plecare.

SELECT 
    Username
FROM
    utilizatori,
    contractare_sejur,
    traseu
WHERE
    (utilizatori.id_utilizator = contractare_sejur.id_utilizator)
        AND contractare_sejur.id_sejur = traseu.id_sejur
HAVING COUNT(traseu.id_traseu) >= 2;

## 7) Toate hotelurile care au un pret mai mare de 150 de euro pe noapte.

SELECT 
    hoteluri.nume
FROM
    hoteluri
WHERE
    hoteluri.pret_pers_noapte > 150;

## 8) Toti angajatii care au salariul mai mare de 3000 de lei, plus functia acestora.

SELECT 
    personal.nume, personal.prenume, personal.functie
FROM
    personal
WHERE
    personal.salariu > 3000;

## 9) Soferii implicati intr-o cursa

SELECT 
    personal.nume, personal.prenume
FROM
    sejur,
    soferi,
    personal
WHERE
    ((sejur.sofer1 = soferi.id_sofer
        OR sejur.sofer2 = soferi.id_sofer)
        AND soferi.id_sofer = personal.id_personal
        AND personal.functie = 'Sofer'
        AND CURRENT_DATE < sejur.data_sfarsit)
GROUP BY soferi.id_sofer;

## 10) Persoanele care au stat la Hotel Nadal si Hotel Levski

SELECT 
    username
FROM
    utilizatori,
    contractare_sejur,
    rezervare_hotel,
    hoteluri
WHERE
    hoteluri.nume = 'Hotel Nadal'
        AND hoteluri.nume = 'Hotel Levski'
        AND hoteluri.id_hotel = rezervare_hotel.id_hotel
        AND contractare_sejur.id_contract = rezervare_hotel.id_contract
        AND utilizatori.id_utilizator = contractare_sejur.id_utilizator; 
        


## 11) Hotelurile de 4 si 5 stele

SELECT 
    hoteluri.nume, hoteluri.NrStele
FROM
    hoteluri
WHERE
    hoteluri.NrStele> 3
        AND hoteluri.NrStele < 6
ORDER BY hoteluri.NrStele;

## 12) Angajatii care sunt in concediu in perioada 1 iulie 2016 - 31 august 2016

SELECT 
    nume, prenume
FROM
    personal,
    concedii
WHERE
    concedii.id_personal = personal.id_personal
        AND ((concedii.Data_start <= '2017/07/31'
        AND concedii.data_start>= '2017/07/01')
        OR (concedii.data_sfarsit > '2017/07/01'
        AND concedii.data_sfarsit <= '2017/07/31'));
        

## 13) Autocarele din sediul Timisoara si Cluj

SELECT 
    sediu.denumire, autocare.marca, autocare.model
FROM
    sediu,
    autocare
WHERE
    autocare.id_sediu = sediu.id_sediu
        AND (sediu.id_sediu = 4 OR sediu.id_sediu = 1);

## 14) Autocarele din Iasi cu mai mult de 50 de locuri

SELECT 
    sediu.denumire,
    autocare.marca,
    autocare.model,
    autocare.Numar_Locuri
FROM
    sediu,
    autocare
WHERE
    autocare.id_sediu = sediu.id_sediu
        AND sediu.id_sediu = 3
        AND autocare.numar_locuri > 50;

## 15) Angajatii din Bucuresti care lucreaza la intretinere

SELECT 
    personal.nume, personal.prenume, personal.functie
FROM
    personal,
    departament,
    sediu
WHERE
    sediu.id_sediu = personal.id_sediu
        AND sediu.denumire = 'GicaWorldTour Bucuresti'
        AND departament.id_departament = personal.id_departament
        AND departament.denumire = 'intretinere';

## 16) Soferii din Cluj si Iasi

SELECT 
    personal.nume, personal.prenume, personal.functie
FROM
    personal,
    sediu
WHERE
    personal.id_sediu = sediu.id_sediu
        AND personal.functie = 'Sofer'
        AND (sediu.id_sediu = 1 OR sediu.id_sediu = 3);


