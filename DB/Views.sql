
## Rolul acestui view este de a furniza informatii in ordine descresctoare despre sejururi, in functie de 
## cati oameni au achizitionat aceste sejururi

CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `sejururi_cumparate` AS
    SELECT 
        `sejur`.`ID_Sejur` AS `ID_Sejur`,
        `sejur`.`ID_Autocar` AS `ID_Autocar`,
        `sejur`.`Sofer1` AS `Sofer1`,
        `sejur`.`Sofer2` AS `Sofer2`,
        `sejur`.`Ghid` AS `Ghid`,
        `sejur`.`Data_start` AS `Data_start`,
        `sejur`.`Data_sfarsit` AS `Data_sfarsit`,
        `sejur`.`Distanta_totala` AS `Distanta_totala`,
        `sejur`.`Pret_transport` AS `Pret_transport`,
        `sejur`.`Nr_locuri` AS `Nr_locuri`,
        `sejur`.`Durata` AS `Durata`
    FROM
        `sejur`
    WHERE
        (`sejur`.`Data_start` > CURDATE())
    ORDER BY (CASE
        WHEN
            `sejur`.`ID_Sejur` IN (SELECT 
                    `contractare_sejur`.`ID_Sejur`
                FROM
                    `contractare_sejur`)
        THEN
            (SELECT 
                    COUNT(`contractare_sejur`.`ID_Contract`)
                FROM
                    `contractare_sejur`
                WHERE
                    (`contractare_sejur`.`ID_Sejur` = `sejur`.`ID_Sejur`))
        WHEN
            (NOT (`sejur`.`ID_Sejur` IN (SELECT 
                    `contractare_sejur`.`ID_Sejur`
                FROM
                    `contractare_sejur`)))
        THEN
            0
    END) DESC;
    
    
## Aceasta vedere are rolul de a mentine o evidenta a cumparaturilor clientilor. Vederea va selecta numele, prenumele
## si punctele de fidelitate ale clientilor, iar pe langa acestea, vederea mai furnizeaza si totalul cumparaturilor 
## acestor clienti
    

CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `clienti` AS
    SELECT 
        `utilizatori`.`Nume` AS `Nume`,
        `utilizatori`.`Prenume` AS `Prenume`,
        `utilizatori`.`Puncte_Fidelitate` AS `Puncte Fidelitate`,
        (CASE
            WHEN
                `utilizatori`.`ID_Utilizator` IN (SELECT 
                        `contractare_sejur`.`ID_Utilizator`
                    FROM
                        `contractare_sejur`)
            THEN
                (SELECT 
                        SUM(`contractare_sejur`.`Pret_Final`)
                    FROM
                        `contractare_sejur`
                    WHERE
                        (`contractare_sejur`.`ID_Utilizator` = `utilizatori`.`ID_Utilizator`))
            WHEN
                (NOT (`utilizatori`.`ID_Utilizator` IN (SELECT 
                        `contractare_sejur`.`ID_Utilizator`
                    FROM
                        `contractare_sejur`)))
            THEN
                0
        END) AS `Total Cumparaturi`,
        (CASE
            WHEN
                `utilizatori`.`ID_Utilizator` IN (SELECT 
                        `contractare_sejur`.`ID_Utilizator`
                    FROM
                        `contractare_sejur`)
            THEN
                (SELECT 
                        SUM(`contractare_sejur`.`Reduceri`)
                    FROM
                        `contractare_sejur`
                    WHERE
                        (`contractare_sejur`.`ID_Utilizator` = `utilizatori`.`ID_Utilizator`))
            WHEN
                (NOT (`utilizatori`.`ID_Utilizator` IN (SELECT 
                        `contractare_sejur`.`ID_Utilizator`
                    FROM
                        `contractare_sejur`)))
            THEN
                0
        END) AS `Total Reduceri`
    FROM
        `utilizatori`
    WHERE
        (`utilizatori`.`Admin` = 0)
    ORDER BY `Total Cumparaturi` DESC;
