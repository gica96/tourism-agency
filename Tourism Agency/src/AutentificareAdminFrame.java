import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class AutentificareAdminFrame extends JPanel implements ActionListener {
	
	private static final long serialVersionUID = 1L;
	static JFrame window;
	
	public static void triggerAutentificareAdmin()
	{
		window = new JFrame("Autentificare_Admin");
		AutentificareAdminFrame content = new AutentificareAdminFrame();
		window.setContentPane(content);
		window.pack();
		window.setLocation(100,100);
		window.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
		window.setVisible(true);
		window.setResizable(true);
		window.setExtendedState(JFrame.MAXIMIZED_BOTH);
	}
	
	JTextField introUsername;
	JTextField introPassword;
	JLabel message;
	
	public AutentificareAdminFrame()
	{
		this.setLayout(new GridBagLayout());
		this.setVisible(true);
		
		JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new GridLayout(4,2));
		centerPanel.setBackground(Color.LIGHT_GRAY);
		
		this.add(centerPanel);
		this.setBackground(Color.LIGHT_GRAY);
		
		JLabel autentificateUsername = new JLabel("Your username");
		centerPanel.add(autentificateUsername);
		
		introUsername = new JTextField();
		centerPanel.add(introUsername);
		
		JLabel autentificarePassword = new JLabel("Your password");
		centerPanel.add(autentificarePassword);
		
		introPassword = new JTextField();
		centerPanel.add(introPassword);
		
		JLabel empty = new JLabel("");
		centerPanel.add(empty);
		
		JButton finishButton = new JButton("Login_As_Admin");
		finishButton.setForeground(Color.RED);
		finishButton.addActionListener(this);
		centerPanel.add(finishButton);
	}
	
	public void actionPerformed(ActionEvent evt)
	{
		String string = evt.getActionCommand();
		Statement selectUsername;
		Statement selectPassword;
		Statement selectAdmin;
		boolean areOk = false;
		ResultSet rsUsername = null;
		ResultSet rsPassword = null;
		ResultSet rsSelectAdmin = null;
		
		if(string.equals("Login_As_Admin"))
		{
			try
			{
				selectUsername = Tourism_Agency.connection.createStatement();
				selectUsername.execute("SELECT utilizatori.username FROM utilizatori");
				rsUsername = selectUsername.getResultSet();
				
			}
			catch(SQLException ex)
			{
				System.out.println("Cannot select utilizatori.username from utilziatori");
			}
			
			try
			{
				selectPassword = Tourism_Agency.connection.createStatement();
				selectPassword.execute("SELECT utilizatori.parola FROM utilizatori");
				rsPassword = selectPassword.getResultSet();
			}
			catch(SQLException ex)
			{
				System.out.println("Cannot select utilizatori.parola from utilizatori");
			}
			
			try
			{
				selectAdmin = Tourism_Agency.connection.createStatement();
				selectAdmin.execute("SELECT utilizatori.admin FROM utilizatori");
				rsSelectAdmin = selectAdmin.getResultSet();
			
			}
			catch(SQLException ex)
			{
				System.out.println("Cannot select utilizatori.admin from utilizatori");
			}
			
			try
			{
				while(rsUsername.next() && rsPassword.next() && rsSelectAdmin.next())
				{
					if(introUsername.getText().equals(rsUsername.getString("Username")))
					{
						if(introPassword.getText().equals(rsPassword.getString("Parola")))
						{
							if(rsSelectAdmin.getInt("Admin") == 1)
							{
								LoggedInAdmin.triggerAdmin();
								areOk = true;
								window.dispose();
								break;
							}
							
						}
					}
				}
				if(areOk == false)
				{
					drawMessage("Invalid username or password or you don't have permission to do that!");
				}
				
			}
			catch(SQLException ex)
			{
				ex.printStackTrace();
			}
			
		}
	}
	
	public void drawMessage(String string)
	{
		Graphics g = this.getGraphics();
		g.setFont(new Font("SERIF",1,25));
		g.drawString(string, 380, 410);
	}
	

}
