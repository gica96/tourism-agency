import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class HoteluriFrame extends JPanel {
	
	private static final long serialVersionUID = 1L;
	
	public static void triggerHoteluri()
	{
		JFrame window = new JFrame("Orasele disponibile");
		HoteluriFrame content = new HoteluriFrame();
		window.setContentPane(content);
		window.pack();
		window.setLocation(100,100);
		window.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
		window.setVisible(true);
		window.setResizable(true);
		window.setExtendedState(JFrame.MAXIMIZED_BOTH);
	}
	
	public HoteluriFrame()
	{
		this.setLayout(new BorderLayout());
		this.setBackground(Color.LIGHT_GRAY);
		
		JPanel northPanel = new JPanel();
		this.add(northPanel, BorderLayout.NORTH);
		
		JLabel message = new JLabel("Mai jos, aveti lista tuturor hotelurilor!");
		northPanel.add(message);
		
		Statement selectHoteluri;
		ResultSet rsSelectHoteluri;
		Statement selectFacilitati;
		ResultSet rsSelectFacilitati;
		Statement countHoteluri;
		ResultSet rsCountHoteluri;
		Statement selectStele;
		ResultSet rsSelectStele;
		
		JPanel centerPanel = new JPanel();
		
		try
		{
			countHoteluri = Tourism_Agency.connection.createStatement();
			countHoteluri.execute("SELECT COUNT(hoteluri.id_hotel) FROM hoteluri");
			rsCountHoteluri = countHoteluri.getResultSet();
			rsCountHoteluri.next();
			centerPanel.setLayout(new GridLayout(rsCountHoteluri.getInt(1),3));
			centerPanel.setBackground(Color.LIGHT_GRAY);
			this.add(centerPanel, BorderLayout.CENTER);
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		}
		
		try
		{
			selectHoteluri = Tourism_Agency.connection.createStatement();
			selectHoteluri.execute("SELECT hoteluri.nume FROM hoteluri");
			rsSelectHoteluri = selectHoteluri.getResultSet();
			
			selectFacilitati = Tourism_Agency.connection.createStatement();
			selectFacilitati.execute("SELECT hoteluri.facilitati FROM hoteluri");
			rsSelectFacilitati = selectFacilitati.getResultSet();
			
			selectStele = Tourism_Agency.connection.createStatement();
			selectStele.execute("SELECT hoteluri.NrStele FROM hoteluri");
			rsSelectStele = selectStele.getResultSet();
			
			while(rsSelectHoteluri.next() && rsSelectFacilitati.next() && rsSelectStele.next())
			{
				JLabel text = new JLabel();
				text.setText(rsSelectHoteluri.getString("Nume")+": ");
				centerPanel.add(text);
				
				JLabel stele = new JLabel();
				stele.setText("Stele: "+rsSelectStele.getInt(1));
				centerPanel.add(stele);
				
				JTextField description = new JTextField();
				description.setText(rsSelectFacilitati.getString("Facilitati"));
				description.setBackground(Color.LIGHT_GRAY);
				centerPanel.add(description);

			}
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		}
	}
}
