import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Statement;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class CreareContFrame extends JPanel implements ActionListener{
	
	private static final long serialVersionUID = 1L;
	
	public static void triggerCreareCont()
	{
		JFrame window = new JFrame("Creare_Cont");
		CreareContFrame content = new CreareContFrame();
		window.setContentPane(content);
		window.pack();
		window.setLocation(100,100);
		window.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
		window.setVisible(true);
		window.setResizable(true);
		window.setExtendedState(JFrame.MAXIMIZED_BOTH);
	}
	
	Graphics image_graphics;
	JTextField textUsername;
	JTextField textPassword;
	JTextField textEmail;
	JTextField textName;
	JTextField textPrenume;
	JTextField textAdresa;
	JTextField textTelefon;
	JTextField textSex;
	JButton finishButton;
	
	public CreareContFrame()
	{
		this.setBackground(Color.CYAN);
		this.setLayout(new BorderLayout());
		
		JPanel southPanel = new JPanel();
		southPanel.setLayout(new GridLayout(9,2,1,1));
		southPanel.setBackground(Color.LIGHT_GRAY);
		this.add(southPanel,BorderLayout.SOUTH);
		
		JLabel username = new JLabel("Username");
		southPanel.add(username);
		
		textUsername = new JTextField();
		southPanel.add(textUsername);
		
		JLabel password = new JLabel("Password");
		southPanel.add(password);
		
		textPassword = new JTextField();
		southPanel.add(textPassword);
		
		JLabel email = new JLabel("Email");
		southPanel.add(email);
		
		textEmail = new JTextField();
		southPanel.add(textEmail);
		
		JLabel name = new JLabel("Name");
		southPanel.add(name);
		
		textName = new JTextField();
		southPanel.add(textName);
		
		JLabel prenume = new JLabel("Prenume");
		southPanel.add(prenume);
		
		textPrenume = new JTextField();
		southPanel.add(textPrenume);
		
		JLabel adresa = new JLabel("Adresa");
		southPanel.add(adresa);
		
		textAdresa = new JTextField();
		southPanel.add(textAdresa);
		
		JLabel telefon = new JLabel("Telefon");
		southPanel.add(telefon);
		
		textTelefon = new JTextField();
		southPanel.add(textTelefon);
		
		JLabel sex = new JLabel("Sex");
		southPanel.add(sex);
		
		textSex = new JTextField();
		southPanel.add(textSex);
		
		JLabel finish = new JLabel("FINISH!");
		southPanel.add(finish);
		
		finishButton = new JButton("Finalizare!");
		finishButton.addActionListener(this);
		southPanel.add(finishButton);
		
		JLabel picLabel;
		
		try
		{
			BufferedImage myPicture = ImageIO.read(new File("C:\\Users\\User\\Desktop\\rome.jpg"));
			picLabel = new JLabel(new ImageIcon(myPicture));
			this.add(picLabel,BorderLayout.CENTER);
			image_graphics = myPicture.getGraphics();
		}
		catch(IOException ex)
		{
			System.out.println("Image was not found!");
	    }
		
	}
	
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
	}
	
	public void actionPerformed(ActionEvent evt)
	{
		String string = evt.getActionCommand();
		Statement insertStatement;
		if(string.equals("Finalizare!"))
		{
			try
			{	
				insertStatement = Tourism_Agency.connection.createStatement();
				insertStatement.execute("INSERT INTO Utilizatori (username,parola,email,nume,prenume,adresa,telefon,sex,admin) VALUES ('"+textUsername.getText()+"','"+textPassword.getText()+"','"+textEmail.getText()+"','"+textName.getText()+"','"+textPrenume.getText()+"','"+textAdresa.getText()+"','"+textTelefon.getText()+"','"+textSex.getText()+"','"+'0'+"')");                                                                                  
			}
			catch(SQLException ex)
			{
				System.out.println("Cannot create statement");
			}
		}
	}
}
