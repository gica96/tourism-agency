import java.awt.Color;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class StergePersonalFrame extends JPanel implements ActionListener{
	
	private static final long serialVersionUID = 1L;
	public static JFrame window;
	
	public static void triggerStergePersonal()
	{
		window = new JFrame("Sterge personal");
		StergePersonalFrame content = new StergePersonalFrame();
		window.setContentPane(content);
		window.pack();
		window.setLocation(100,100);
		window.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
		window.setVisible(true);
		window.setResizable(true);
		window.setExtendedState(JFrame.MAXIMIZED_BOTH);
	}
	
	JTextField numePersonal;
	JTextField prenumePersonal;
	
	public StergePersonalFrame()
	{
		this.setLayout(new GridBagLayout());
		this.setVisible(true);
		
		JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new GridLayout(4,2));
		centerPanel.setBackground(Color.LIGHT_GRAY);
		
		this.add(centerPanel);
		this.setBackground(Color.LIGHT_GRAY);
		
		JLabel numePersonalL = new JLabel("Numele Angajatului: ");
		centerPanel.add(numePersonalL);
		
		numePersonal = new JTextField();
		centerPanel.add(numePersonal);
		
		JLabel prenumePersonalL = new JLabel("Prenumele Angajatului: ");
		centerPanel.add(prenumePersonalL);
		
		prenumePersonal = new JTextField();
		centerPanel.add(prenumePersonal);
		
		JLabel empty = new JLabel("");
		centerPanel.add(empty);
		
		JButton deleteButton = new JButton("Sterge Angajat!");
		deleteButton.setForeground(Color.BLUE);
		deleteButton.addActionListener(this);
		centerPanel.add(deleteButton);
		
	}
	
	public void actionPerformed(ActionEvent evt)
	{
		String string = evt.getActionCommand();
		if(string.equals("Sterge Angajat!"))
		{
			try
			{
				Statement deleteStatement = Tourism_Agency.connection.createStatement();
				deleteStatement.execute("DELETE FROM personal WHERE personal.nume='"+numePersonal.getText()+"' AND personal.prenume='"+prenumePersonal.getText()+"'");           
				window.dispose();
			}
			catch(SQLException ex)
			{
				ex.printStackTrace();
			}
		}
	}
}
