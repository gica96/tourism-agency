import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Tourism_Agency extends JPanel implements ActionListener{
	
	public static final String URL = "jdbc:mysql://localhost/agentie_turism";
	public static final String PASS = "root";
	public static final String USER = "root";
	private static final long serialVersionUID = 1L;
	
	public static void main(String[] args)
	{
		
		JFrame window = new JFrame("Tourism_Agency");
		Tourism_Agency content = new Tourism_Agency();
		window.setContentPane(content);
		window.pack();
		window.setLocation(100,100);
		window.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		window.setVisible(true);
		window.setResizable(true);
		window.setExtendedState(JFrame.MAXIMIZED_BOTH);
		
	}
	
	static Connection connection = null;
	static Statement selectStatement = null;
	static ResultSet rs = null;
	static ResultSetMetaData rsmd = null;
	Graphics image_graphics;
	
	public Tourism_Agency()
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
		}
		catch(ClassNotFoundException ex)
		{
			System.out.println("Unable to load the driver class!");
		}
		catch(IllegalAccessException ex)
		{
			System.out.println("Acces problem with loading!");
		}
		catch(InstantiationException ex)
		{
			System.out.println("Unnable to instantiate the driver!");
		}
		
		try
		{
			connection = DriverManager.getConnection(URL,USER,PASS);
		}
		catch(SQLException ex)
		{
			System.out.println("Some error has occured!");
		}
		
		this.setBackground(Color.CYAN);
		this.setLayout(new BorderLayout());
		JPanel southPanel = new JPanel();
		southPanel.setLayout(new GridLayout(1,7));
		this.add(southPanel,BorderLayout.SOUTH);
		
		JButton create_user = new JButton("Creare_Cont");
		create_user.addActionListener(this);
		southPanel.add(create_user);
		
		JButton login = new JButton("Autentificare");
		login.addActionListener(this);
		southPanel.add(login);
		
		JButton login_admin = new JButton("Autentificare_Admin");
		login_admin.addActionListener(this);
		southPanel.add(login_admin);
		
		JButton orase = new JButton("Orase");
		orase.addActionListener(this);
		southPanel.add(orase);
		
		JButton hoteluri = new JButton("Hoteluri");
		hoteluri.addActionListener(this);
		southPanel.add(hoteluri);
		
		JButton sejururi = new JButton("Obiective_Turistice");
		sejururi.addActionListener(this);
		southPanel.add(sejururi);
		
		
		
		JLabel picLabel;

		try
		{
			BufferedImage myPicture = ImageIO.read(new File("C:\\Users\\User\\Desktop\\Tropical-Palm-beach-1366x768.jpg"));
			picLabel = new JLabel(new ImageIcon(myPicture));
			this.add(picLabel,BorderLayout.CENTER);
			image_graphics = myPicture.getGraphics();
		}
		catch(IOException ex)
		{
			System.out.println("Image was not found!");
		}
		
		
		
	}
	
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g = image_graphics;
		g.setFont(new Font("SERIF",1,48));
		g.setColor(Color.RED);
		g.drawString("Bun venit pe site-ul agentiei de turism GicaWorldTour!", 90,100);
		
		g.setFont(new Font("SERIF",1,40));
		g.setColor(Color.yellow);
		g.drawString("Pentru a va crea un cont nou, apasati butonul >>Creare_Cont<<",90,250);
		
		g.drawString("Pentru a va autentifica, apasati butonul >>Autentificare<<",90,300);
		
		g.drawString("Pentru a va autentifica ca admin, apasati butonul >>Autentificare_Admin<<",50,350);
		
		g.drawString("Pentru a vedea orasele disponibile, apsati butonul >>Orase<<",90,400);
		
		g.drawString("Pentru a vedea hotelurile, apasati butonul >>Hoteluri<<",90,450);
		
		g.drawString("Pentru a vedea obiectivele, apasati butonul >>Obiective<<",90,500);
		
	}
	public void actionPerformed(ActionEvent evt)
	{
		String event = evt.getActionCommand();
		if (event.equals("Creare_Cont"))
		{
			doCreareCont();
		}
		else if(event.equals("Autentificare"))
		{
			doAutentificare();
		}
		else if(event.equals("Autentificare_Admin"))
		{
			doAutentificareAdmin();
		}
		else if(event.equals("Orase"))
		{
			doOrase();
		}
		else if(event.equals("Hoteluri"))
		{
			doHoteluri();
		}
		else if(event.equals("Obiective_Turistice"))
		{
			doObiective();
		}
		
	}
	
	public void doCreareCont()
	{
		CreareContFrame.triggerCreareCont();
	}
	
	public void doAutentificare()
	{
		AutentificareFrame.triggerAutentificare();
	}
	
	public void doOrase()
	{
		OraseFrame.triggerOrase();
	}
	
	public void doHoteluri()
	{
		HoteluriFrame.triggerHoteluri();
	}
	
	public void doObiective()
	{
		ObiectiveFrame.triggerObiective();
	}
	
	public void doAutentificareAdmin()
	{
		AutentificareAdminFrame.triggerAutentificareAdmin();
	}

}
