import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class PretFinalFrame extends JPanel {
	
	private static final long serialVersionUID = 1L;
	
	public static void triggerPretFinal()
	{
		JFrame window = new JFrame("Logged In");
		PretFinalFrame content = new PretFinalFrame();
		window.setContentPane(content);
		window.pack();
		window.setLocation(100,100);
		window.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
		window.setVisible(true);
		window.setResizable(true);
		window.setExtendedState(JFrame.MAXIMIZED_BOTH);
	}
	
	public PretFinalFrame()
	{
		this.setBackground(Color.LIGHT_GRAY);
		this.setLayout(new BorderLayout());
	
		JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new GridLayout(10,1));
		centerPanel.setBackground(Color.LIGHT_GRAY);
		this.add(centerPanel,BorderLayout.CENTER);
		
		String utilizator;
		Statement selectId;
		Statement selectIdSejur;
		Statement selectPretFinal;
		ResultSet rsId = null;
		ResultSet rsIdSejur = null;
		ResultSet rsPretFinal = null;
		int idUtilizator = 0;
		
		
		try
		{
			utilizator = AutentificareFrame.copyResultSet.getString("Username");
			selectId = Tourism_Agency.connection.createStatement();
			selectId.execute("SELECT utilizatori.Id_Utilizator FROM utilizatori WHERE utilizatori.Username= '"+utilizator+"'");
			rsId = selectId.getResultSet();
			rsId.next();
			idUtilizator = rsId.getInt(1);
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		}
		
		try
		{
			selectIdSejur = Tourism_Agency.connection.createStatement();
			selectIdSejur.execute("SELECT contractare_sejur.Id_Sejur FROM contractare_sejur WHERE contractare_sejur.id_utilizator= '"+idUtilizator+"'");
			rsIdSejur = selectIdSejur.getResultSet();
		
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		}
		
		try
		{
			selectPretFinal = Tourism_Agency.connection.createStatement();
			selectPretFinal.execute("SELECT contractare_sejur.pret_final FROM contractare_sejur WHERE contractare_sejur.id_utilizator= '"+idUtilizator+"'");
			rsPretFinal = selectPretFinal.getResultSet();
			
		
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		}
		
		
	try
	{
		if(rsIdSejur.next() == false)
		{
			JLabel infoPreturi = new JLabel();
			infoPreturi.setText("Nu ati cumparat momentan niciun sejur!");
			centerPanel.add(infoPreturi);
		}
		else
		{
			rsIdSejur.beforeFirst();
			while(rsIdSejur.next() && rsPretFinal.next())
			{
				JLabel info = new JLabel();
				info.setText("Pentru sejurul "+rsIdSejur.getInt("ID_Sejur")+" aveti de platit: "+rsPretFinal.getString("Pret_Final")+ " lei");
				centerPanel.add(info);
			}
		}
	}
	catch(SQLException ex)
	{
		ex.printStackTrace();
	}
		
		
		
		
		
		
		
		
		
	}
}
