import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class ListUtilizatoriFrame extends JPanel {
	
	private static final long serialVersionUID = 1L;
	
	public static void triggerUtilizatori()
	{
		JFrame window = new JFrame("Toti utilizatorii");
		ListUtilizatoriFrame content = new ListUtilizatoriFrame();
		window.setContentPane(content);
		window.pack();
		window.setLocation(100,100);
		window.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
		window.setVisible(true);
		window.setResizable(true);
		window.setExtendedState(JFrame.MAXIMIZED_BOTH);
	}
	
	public ListUtilizatoriFrame()
	{
		this.setBackground(Color.LIGHT_GRAY);
		this.setLayout(new BorderLayout());
		
		JPanel northPanel = new JPanel();
		this.add(northPanel,BorderLayout.NORTH);
		
		JLabel welcomeMessage = new JLabel("Aici vezi toti utilizatorii inregistrati pana acum!");
		northPanel.add(welcomeMessage);
		
		JPanel centerPanel = new JPanel();
		
		Statement countIdUtilizator;
		ResultSet rsCountIdUtilizator;
		
		Statement selectUtilizator;
		ResultSet rsSelectUtilizator;
		
		try
		{
			countIdUtilizator = Tourism_Agency.connection.createStatement();
			countIdUtilizator.execute("SELECT COUNT(utilizatori.id_utilizator) FROM utilizatori");
			rsCountIdUtilizator = countIdUtilizator.getResultSet();
			rsCountIdUtilizator.next();
			centerPanel.setLayout(new GridLayout(rsCountIdUtilizator.getInt(1),10));
			centerPanel.setBackground(Color.LIGHT_GRAY);
			this.add(centerPanel, BorderLayout.CENTER);
			
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		}
		
		try
		{
			selectUtilizator = Tourism_Agency.connection.createStatement();
			selectUtilizator.execute("SELECT*FROM utilizatori");
			rsSelectUtilizator = selectUtilizator.getResultSet();
			while(rsSelectUtilizator.next())
			{
				JLabel username = new JLabel();
				username.setText(rsSelectUtilizator.getString("Username"));
				centerPanel.add(username);
				
				JLabel email = new JLabel();
				email.setText("Email: "+rsSelectUtilizator.getString("Email"));
				centerPanel.add(email);
				
				JLabel nume = new JLabel();
				nume.setText("Nume: "+rsSelectUtilizator.getString("Nume"));
				centerPanel.add(nume);
				
				JLabel prenume = new JLabel();
				prenume.setText("Prenume: "+rsSelectUtilizator.getString("Prenume"));
				centerPanel.add(prenume);
				
				JLabel adresa = new JLabel();
				adresa.setText("Adresa: "+rsSelectUtilizator.getString("Adresa"));
				centerPanel.add(adresa);
				
				JLabel telefon = new JLabel();
				telefon.setText("Telefon: "+rsSelectUtilizator.getString("Telefon"));
				centerPanel.add(telefon);
				
				JLabel sex = new JLabel();
				sex.setText("Sex: "+rsSelectUtilizator.getString("Sex"));
				centerPanel.add(sex);
				
				JLabel puncte = new JLabel();
				puncte.setText("P.F: "+rsSelectUtilizator.getString("Puncte_Fidelitate"));
				centerPanel.add(puncte);
				
				
				
			}
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		}
	
	}
}
