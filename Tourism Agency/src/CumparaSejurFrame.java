import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class CumparaSejurFrame extends JPanel implements ActionListener {
	
	private static final long serialVersionUID = 1L;
	public static String hotelName;


	
	public static void triggerCumparaSejur()
	{
		JFrame window = new JFrame("Cumpara Sejururi!");
		CumparaSejurFrame content = new CumparaSejurFrame();
		window.setContentPane(content);
		window.pack();
		window.setLocation(100,100);
		window.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
		window.setVisible(true);
		window.setResizable(true);
		window.setExtendedState(JFrame.MAXIMIZED_BOTH);
	}
	
	static int whichButton;
	static int howManySejururi;
	Statement countNrSejururi;
	Statement selectOrase;
	Statement selectHoteluri;
	Statement selectPretTransport;
	Statement selectDataStart;
	Statement selectDataSfarsit;
	Statement selectLocuri;
	ResultSet rsPretTransport;
	ResultSet rsNrSejururi;
	ResultSet rsOrase;
	ResultSet rsDataStart;
	ResultSet rsDataSfarsit;
	ResultSet rsLocuri;
	
	public static ResultSet rsHoteluri;
	private int nrSejururi;
	ArrayList<JButton> buyButton = new ArrayList<JButton>();
	ArrayList<JTextField> nrOfSejururi = new ArrayList<JTextField>();
	
	public CumparaSejurFrame()
	{
		this.setLayout(new BorderLayout());
		this.setBackground(Color.CYAN);
		
		JPanel northPanel = new JPanel();
		this.add(northPanel,BorderLayout.NORTH);
		
		JLabel message = new JLabel();
		message.setFont(new Font("SARIF",1,20));
		message.setText("Alegeti sejurul dorit!");
		northPanel.add(message);
		
		JPanel centerPanel = new JPanel();
		centerPanel.setBackground(Color.CYAN);
		this.add(centerPanel, BorderLayout.CENTER);
		
		try
		{
			countNrSejururi = Tourism_Agency.connection.createStatement();
			countNrSejururi.execute("SELECT COUNT(ID_Sejur) FROM sejur");
			rsNrSejururi = countNrSejururi.getResultSet();
			rsNrSejururi.next();
			nrSejururi = rsNrSejururi.getInt(1);
			
		}
		catch(SQLException ex)
		{
			System.out.println("Cannot count id_sejur");
		}
		
		try
		{
			centerPanel.setLayout(new GridLayout(rsNrSejururi.getInt(1),7));
		}
		catch(SQLException ex)
		{
			System.out.println("Cannot initialize centerPanel's layout!");
		}
		
		try
		{
			selectOrase = Tourism_Agency.connection.createStatement();
			selectOrase.execute("SELECT orase.denumire FROM orase,sejur,traseu WHERE sejur.id_sejur = traseu.id_sejur AND traseu.oras1 = orase.id_oras");
			rsOrase = selectOrase.getResultSet();
			rsOrase.next();
		}
		catch(SQLException ex)
		{
			System.out.println("Eroare");
		}
		
		try
		{
			selectPretTransport = Tourism_Agency.connection.createStatement();
			selectPretTransport.execute("SELECT sejur.pret_transport from sejur");
			rsPretTransport = selectPretTransport.getResultSet();
			rsPretTransport.next();
		}
		catch(SQLException ex)
		{
			System.out.println("Cannot select pret_transport");
		}
		
		try
		{
			selectDataStart = Tourism_Agency.connection.createStatement();
			selectDataStart.execute("SELECT sejur.Data_start from sejur");
			rsDataStart = selectDataStart.getResultSet();
			rsDataStart.next();
		}
		catch(SQLException ex)
		{
			System.out.println("Cannot select data_Start");
		}
		
		try
		{
			selectDataSfarsit = Tourism_Agency.connection.createStatement();
			selectDataSfarsit.execute("SELECT sejur.Data_sfarsit from sejur");
			rsDataSfarsit = selectDataSfarsit.getResultSet();
			rsDataSfarsit.next();
		}
		catch(SQLException ex)
		{
			System.out.println("Cannot select data_Start");
		}
		
		try
		{
			selectLocuri = Tourism_Agency.connection.createStatement();
			selectLocuri.execute("SELECT sejur.nr_locuri from sejur");
			rsLocuri = selectLocuri.getResultSet();
			rsLocuri.next();
		}
		catch(SQLException ex)
		{
			System.out.println("Cannot select data_Start");
		}
		
		try
		{
			for(int i=1;i<=nrSejururi;i++)
			{
				
				JTextField infoSejur = new JTextField();
				infoSejur.setFont(new Font("SARIF",1,15));
				infoSejur.setText("Sejur"+i+" "+":"+rsOrase.getString(1));
				rsOrase.next();
				infoSejur.setText(infoSejur.getText()+"-"+rsOrase.getString(1));
				centerPanel.add(infoSejur);
				rsOrase.next();
				
				JLabel pretTransportL = new JLabel();
				pretTransportL.setFont(new Font("SARIF",1,13));
				pretTransportL.setText("Pretul transportului: "+rsPretTransport.getString("Pret_Transport"));
				centerPanel.add(pretTransportL);
				rsPretTransport.next();
				
				JLabel dataStartL = new JLabel();
				dataStartL.setFont(new Font("SARIF",1,13));
				dataStartL.setText("Data_Start: "+rsDataStart.getDate("Data_start"));
				centerPanel.add(dataStartL);
				rsDataStart.next();
				
				JLabel dataSfarsitL = new JLabel();
				dataSfarsitL.setFont(new Font("SARIF",1,13));
				dataSfarsitL.setText("Data_Sfarsit: "+rsDataSfarsit.getDate("Data_sfarsit"));
				centerPanel.add(dataSfarsitL);
				rsDataSfarsit.next();
				
				JLabel locuriL = new JLabel();
				locuriL.setFont(new Font("SARIF",1,13));
				locuriL.setText("Locuri: "+rsLocuri.getInt("Nr_locuri"));
				centerPanel.add(locuriL);
				rsLocuri.next();
				
				JLabel numarSejururi = new JLabel();
				numarSejururi.setFont(new Font("SARIF",1,13));
				numarSejururi.setText("Numarul de sejururi:");
				centerPanel.add(numarSejururi);
				
				JTextField numarSejururiCumparate;
				numarSejururiCumparate = new JTextField();
				nrOfSejururi.add(numarSejururiCumparate);
				centerPanel.add(numarSejururiCumparate);
				
				JButton cumparaSejur = new JButton("Cumpara!");
				buyButton.add(cumparaSejur);
				cumparaSejur.addActionListener(this);
				centerPanel.add(cumparaSejur);
			
			}
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		}
		
		

		
		
	}
	
	public void actionPerformed(ActionEvent evt)
	{
		
		
		
		for(JButton buton : buyButton)
		{
			if(buton.equals((JButton)evt.getSource()))
			{
				
				try
				{
					
					whichButton = buyButton.indexOf(buton)+1;
					selectHoteluri = Tourism_Agency.connection.createStatement();
					selectHoteluri.execute("SELECT hoteluri.nume FROM hoteluri, sejur, traseu WHERE sejur.id_sejur='"+whichButton+"' AND hoteluri.oras = traseu.oras1 AND traseu.id_sejur = sejur.id_sejur");
					rsHoteluri = selectHoteluri.getResultSet();
					
					howManySejururi = Integer.parseInt(nrOfSejururi.get(whichButton-1).getText());
					if(howManySejururi<=0)
					{
						System.out.println("Nu puteti introduce la numarul de sejururi cumparate 0 sau numar negativ!");
						System.exit(-1);
					}
					else
					{
						SelectHoteluriFrame.triggerSelectHoteluri();
						break;
					}
					
				}
				catch(Exception ex)
				{
					System.out.println("Nu ati selectat numarul de sejururi dorite!");
					System.exit(-1);
				}
				
			}
		}
	}
	
	public static void doContinue()
	{
		
		Statement selectId;
		ResultSet rsId;
		int idUtilizator = 0;
		String utilizator;
		Statement selectHotelId;
		ResultSet rsHotelId = null;
		CallableStatement callableStatement;
		
		
		hotelName = SelectHoteluriFrame.hotelName;
		
		try
		{
			utilizator = AutentificareFrame.copyResultSet.getString("Username");
			selectId = Tourism_Agency.connection.createStatement();
			selectId.execute("SELECT utilizatori.Id_Utilizator FROM utilizatori WHERE utilizatori.Username= '"+utilizator+"'");
			rsId = selectId.getResultSet();
			rsId.next();
			idUtilizator = rsId.getInt(1);
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		}
		
		try
		{
			selectHotelId = Tourism_Agency.connection.createStatement();
			selectHotelId.execute("SELECT hoteluri.id_hotel FROM hoteluri WHERE hoteluri.nume='"+hotelName+"'");
			rsHotelId = selectHotelId.getResultSet();
			rsHotelId.next();
						
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		}
		
		try
		{
			
			callableStatement = Tourism_Agency.connection.prepareCall("{call cumparare_sejur(?,?,?,?)}");
			callableStatement.setInt(1,idUtilizator);
			callableStatement.setInt(2, whichButton);
			callableStatement.setInt(3, howManySejururi);
			callableStatement.setString(4,rsHotelId.getString(1)+",");
			callableStatement.executeQuery();
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		}
		
		
	}

}
