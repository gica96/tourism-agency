import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class LoggedIn extends JPanel implements ActionListener {
	
	private static final long serialVersionUID = 1L;

	
	public static void triggerLogin()
	{
		JFrame window = new JFrame("Logged In");
		LoggedIn content = new LoggedIn();
		window.setContentPane(content);
		window.pack();
		window.setLocation(100,100);
		window.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
		window.setVisible(true);
		window.setResizable(true);
		window.setExtendedState(JFrame.MAXIMIZED_BOTH);
	}
	
	public LoggedIn()
	{
		this.setBackground(Color.LIGHT_GRAY);
		this.setLayout(new BorderLayout());
		
		JPanel northPanel = new JPanel();
		northPanel.setLayout(new GridBagLayout());
		this.add(northPanel,BorderLayout.NORTH);
		
		JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new GridBagLayout());
		centerPanel.setBackground(Color.LIGHT_GRAY);
		this.add(centerPanel, BorderLayout.CENTER);
		
		JButton puncteFidelitate = new JButton("Puncte Fidelitate");
		puncteFidelitate.setForeground(Color.BLUE);
		puncteFidelitate.addActionListener(this);
		centerPanel.add(puncteFidelitate);
		
		JButton buyIt = new JButton("Cumpara Sejururi!");
		buyIt.setForeground(Color.BLUE);
		buyIt.addActionListener(this);
		centerPanel.add(buyIt);
		
		JButton pretFinal = new JButton("Pret Final");
		pretFinal.setForeground(Color.BLUE);
		pretFinal.addActionListener(this);
		centerPanel.add(pretFinal);
		
		try
		{
			JLabel welcomeLabel = new JLabel();
			welcomeLabel.setText("Bine ati venit, "+ AutentificareFrame.copyResultSet.getString("Username")+"! "
					+"Mai jos, aveti optiunile in functie de ce doriti sa faceti! ");
			welcomeLabel.setFont(new Font("SERIF",1,20));
			northPanel.add(welcomeLabel);
		}
		catch(SQLException ex)
		{
			System.out.println("Error");
		}
	}
	
	
	
	public void actionPerformed(ActionEvent evt)
	{
		String string = evt.getActionCommand();
		if(string.equals("Puncte Fidelitate"))
		{
			doPuncteFidelitate();
		}
		else if(string.equals("Cumpara Sejururi!"))
		{
			doCumparaSejururi();
		}
		else if(string.equals("Pret Final"))
		{
			doPretFinal();
		}
	}
	
	public void doPuncteFidelitate()
	{
		PuncteFidelitateFrame.triggerPuncteFidelitate();
	}
	
	public void doCumparaSejururi()
	{
		CumparaSejurFrame.triggerCumparaSejur();
	}
	
	public void doPretFinal()
	{
		PretFinalFrame.triggerPretFinal();
	}

}
