import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.CallableStatement;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class LoggedInAdmin extends JPanel implements ActionListener {
	
	private static final long serialVersionUID = 1L;
	
	public static void triggerAdmin()
	{
		JFrame window = new JFrame("Logged In Admin");
		LoggedInAdmin content = new LoggedInAdmin();
		window.setContentPane(content);
		window.pack();
		window.setLocation(100,100);
		window.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
		window.setVisible(true);
		window.setResizable(true);
		window.setExtendedState(JFrame.MAXIMIZED_BOTH);
	}
	
	public LoggedInAdmin()
	{
		this.setBackground(Color.LIGHT_GRAY);
		this.setLayout(new BorderLayout());
		
		JPanel northPanel = new JPanel();
		this.add(northPanel,BorderLayout.NORTH);
		
		JLabel welcomeMessage = new JLabel("Salut, George!");
		northPanel.add(welcomeMessage);
		
		JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new GridBagLayout());
		centerPanel.setBackground(Color.LIGHT_GRAY);
		this.add(centerPanel,BorderLayout.CENTER);
		
		JButton listUsers = new JButton("Listeaza Utilizatorii");
		listUsers.addActionListener(this);
		centerPanel.add(listUsers);
		
		JButton updateAutocare = new JButton("Update Autocare");
		updateAutocare.addActionListener(this);
		centerPanel.add(updateAutocare);
		
		JButton updateSoferi = new JButton("Update Soferi");
		updateSoferi.addActionListener(this);
		centerPanel.add(updateSoferi);
		
		JButton addEmployee = new JButton("Adauga personal");
		addEmployee.addActionListener(this);
		centerPanel.add(addEmployee);
		
		JButton deleteEmployee = new JButton("Sterge personal");
		deleteEmployee.addActionListener(this);
		centerPanel.add(deleteEmployee);
	}
	
	public void actionPerformed(ActionEvent evt)
	{
		String string = evt.getActionCommand();
		if(string.equals("Listeaza Utilizatorii"))
		{
			doListeazaUtilizatori();
		}
		else if(string.equals("Update Autocare"))
		{
			doUpdateAutocare();
		}
		else if(string.equals("Update Soferi"))
		{
			doUpdateSoferi();
		}
		else if(string.equals("Adauga personal"))
		{
			doAdaugaPersonal();
		}
		else if(string.equals("Sterge personal"))
		{
			doStergePersonal();
		}
	}
	
	public void doListeazaUtilizatori()
	{
		ListUtilizatoriFrame.triggerUtilizatori();
	}
	
	public void doUpdateAutocare()
	{
		boolean ok = true;
		try
		{
			CallableStatement updateAutocare = null;
			updateAutocare = Tourism_Agency.connection.prepareCall("{call update_autocare_procedure}");
			updateAutocare.executeQuery();
		}
		catch(SQLException ex)
		{
			ok = false;
			ex.printStackTrace();
		}
		finally
		{
			if(ok)
			{
				System.out.println("Succes! Pentru ca efectul procedurii sa aiba loc, e necesar ca sejururile in care autocarele sunt angrenate, sa se fi terminat!");
			}
		}
	}
	
	public void doUpdateSoferi()
	{
		boolean ok = true;
		try
		{
			CallableStatement updateSoferi = null;
			updateSoferi = Tourism_Agency.connection.prepareCall("{call update_soferi_procedure}");
			updateSoferi.executeQuery();
		}
		catch(SQLException ex)
		{
			ok = false;
			ex.printStackTrace();
		}
		finally
		{
			if(ok)
			{
				System.out.println("Succes! Pentru ca efectul procedurii sa aiba loc, e necesar ca sejururile in care soferii sunt angrenati, sa se fi terminat!");
			}
		}
	}
	
	public void doAdaugaPersonal()
	{
		AdaugaPersonalFrame.triggerAdaugaPersonal();
	}
	
	public void doStergePersonal()
	{
		StergePersonalFrame.triggerStergePersonal();
	}
}
