import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class AdaugaPersonalFrame extends JPanel implements ActionListener{
	
	private static final long serialVersionUID = 1L;
	
	public static JFrame window;
	public static void triggerAdaugaPersonal()
	{
		window = new JFrame("Adauga Personal");
		AdaugaPersonalFrame content = new AdaugaPersonalFrame();
		window.setContentPane(content);
		window.pack();
		window.setLocation(100,100);
		window.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
		window.setVisible(true);
		window.setResizable(true);
		window.setExtendedState(JFrame.MAXIMIZED_BOTH);
	}
	
	JTextField nume;
	JTextField prenume;
	JTextField adresa;
	JTextField telefon;
	JTextField functie;
	JTextField salariu;
	JTextField idSediu;
	JTextField idDepartament;
	
	public AdaugaPersonalFrame()
	{
		this.setLayout(new BorderLayout());
		this.setBackground(Color.LIGHT_GRAY);
		
		JPanel centerPanel = new JPanel();
		centerPanel.setBackground(Color.LIGHT_GRAY);
		centerPanel.setLayout(new GridLayout(9,2));
		this.add(centerPanel, BorderLayout.CENTER);
		
		JLabel numeL = new JLabel("Nume:");
		centerPanel.add(numeL);
		nume = new JTextField();
		centerPanel.add(nume);
		
		JLabel prenumeL = new JLabel("Prenume:");
		centerPanel.add(prenumeL);
		prenume = new JTextField();
		centerPanel.add(prenume);
		
		JLabel adresaL = new JLabel("Adresa:");
		centerPanel.add(adresaL);
		adresa = new JTextField();
		centerPanel.add(adresa);
		
		JLabel telefonL = new JLabel("Telefon:");
		centerPanel.add(telefonL);
		telefon = new JTextField();
		centerPanel.add(telefon);
		
		JLabel functieL = new JLabel("Functie:");
		centerPanel.add(functieL);
		functie = new JTextField();
		centerPanel.add(functie);
		
		JLabel salariuL = new JLabel("Salariu:");
		centerPanel.add(salariuL);
		salariu = new JTextField();
		centerPanel.add(salariu);
	
		
		JLabel idSediuL = new JLabel("Sediu:");
		centerPanel.add(idSediuL);
		idSediu = new JTextField();
		centerPanel.add(idSediu);
		
		JLabel idDepartamentL = new JLabel("Departament:");
		centerPanel.add(idDepartamentL);
		idDepartament = new JTextField();
		centerPanel.add(idDepartament);
		
		JButton insertButton = new JButton("Adauga!");
		insertButton.setForeground(Color.BLUE);
		insertButton.addActionListener(this);
		this.add(insertButton, BorderLayout.SOUTH);
	}
	
	public void actionPerformed(ActionEvent evt)
	{
		String string = evt.getActionCommand();
		if(string.equals("Adauga!"))
		{
			Date date = new Date();
		
			try
			{
				String sql = "INSERT INTO personal(Nume,Prenume,Adresa,Telefon,Functie,Salariu,Data_angajarii,ID_Sediu,ID_Departament) VALUES(?,?,?,?,?,?,?,?,?)";
				PreparedStatement stmt = Tourism_Agency.connection.prepareStatement(sql);
				stmt.setString(1, nume.getText());
				stmt.setString(2, prenume.getText());
				stmt.setString(3, adresa.getText());
				stmt.setString(4, telefon.getText());
				stmt.setString(5, functie.getText());
				stmt.setFloat(6, Float.parseFloat(salariu.getText()));
				stmt.setDate(7,new java.sql.Date(date.getTime()));
				stmt.setInt(8, Integer.parseInt(idSediu.getText()));
				stmt.setInt(9, Integer.parseInt(idDepartament.getText()));
				stmt.execute();
				window.dispose();
			}
			catch(SQLException ex)
			{
				ex.printStackTrace();
			}
		
		}
	}

}
