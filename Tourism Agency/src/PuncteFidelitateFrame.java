import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class PuncteFidelitateFrame extends JPanel {
	
	private static final long serialVersionUID = 1L;
	
	public static void triggerPuncteFidelitate()
	{
		JFrame window = new JFrame("Puncte Fidelitate");
		PuncteFidelitateFrame content = new PuncteFidelitateFrame();
		window.setContentPane(content);
		window.pack();
		window.setLocation(100,100);
		window.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
		window.setVisible(true);
		window.setResizable(true);
		window.setExtendedState(JFrame.MAXIMIZED_BOTH);
	}
	
	public PuncteFidelitateFrame()
	{
		this.setBackground(Color.LIGHT_GRAY);
		this.setLayout(new BorderLayout());
	
		JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new GridBagLayout());
		centerPanel.setBackground(Color.LIGHT_GRAY);
		this.add(centerPanel,BorderLayout.CENTER);
		
		try
		{
			String username = AutentificareFrame.copyResultSet.getString("Username");
			Statement getPuncteFidelitate = Tourism_Agency.connection.createStatement();
			ResultSet rsPuncteFidelitate;
			getPuncteFidelitate.execute("SELECT utilizatori.Puncte_Fidelitate FROM utilizatori WHERE utilizatori.Username = '" + username + "'");                            
			rsPuncteFidelitate = getPuncteFidelitate.getResultSet();
			rsPuncteFidelitate.next();
			
			JLabel message = new JLabel();
			message.setFont(new Font("SARIF",1,25));
			message.setText("Momentan aveti: " + rsPuncteFidelitate.getString("Puncte_Fidelitate") + " puncte fidelitate");
			centerPanel.add(message);
			
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		}
		
		
	}

}
