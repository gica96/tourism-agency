import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class OraseFrame extends JPanel{
	
	private static final long serialVersionUID = 1L;

	
	public static void triggerOrase()
	{
		JFrame window = new JFrame("Orasele disponibile");
		OraseFrame content = new OraseFrame();
		window.setContentPane(content);
		window.pack();
		window.setLocation(100,100);
		window.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
		window.setVisible(true);
		window.setResizable(true);
		window.setExtendedState(JFrame.MAXIMIZED_BOTH);
	}
	
	public OraseFrame()
	{
		this.setLayout(new BorderLayout());
		this.setBackground(Color.CYAN);
		
		JPanel northPanel = new JPanel();
		this.add(northPanel, BorderLayout.NORTH);
		
		JLabel message = new JLabel("Mai jos, aveti lista de orase disponibile momentan!");
		northPanel.add(message);
		
		ResultSet rsCountIdOrase;
		Statement countIdOrase;
		
		ResultSet rsSelectOrase;
		Statement selectOrase;
		
		ResultSet rsSelectDescriere;
		Statement selectDescriere;
		
		JPanel centerPanel = new JPanel();
		
		try
		{
			countIdOrase = Tourism_Agency.connection.createStatement();
			countIdOrase.execute("SELECT COUNT(orase.Id_Oras) FROM orase");
			rsCountIdOrase = countIdOrase.getResultSet();
			rsCountIdOrase.next();
			
			centerPanel.setLayout(new GridLayout(rsCountIdOrase.getInt(1),2));
			centerPanel.setBackground(Color.CYAN);
			this.add(centerPanel, BorderLayout.CENTER);
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		}
		
		try
		{
			selectOrase = Tourism_Agency.connection.createStatement();
			selectOrase.execute("SELECT orase.denumire FROM orase");
			rsSelectOrase = selectOrase.getResultSet();
			
			selectDescriere = Tourism_Agency.connection.createStatement();
			selectDescriere.execute("SELECT orase.descriere FROM orase");
			rsSelectDescriere = selectDescriere.getResultSet();
			
			while(rsSelectOrase.next() && rsSelectDescriere.next())
			{
				JLabel text = new JLabel();
				text.setText(rsSelectOrase.getString("Denumire")+": ");
				centerPanel.add(text);
				
				JTextField description = new JTextField();
				description.setText(rsSelectDescriere.getString("Descriere"));
				description.setBackground(Color.CYAN);
				centerPanel.add(description);

				
			}
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		}
	
	}

}
