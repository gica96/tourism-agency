import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class SelectHoteluriFrame extends JPanel implements ActionListener {
	
	private static final long serialVersionUID = 1L;
	public static String hotelName;
	static JFrame window;
	
	public static void triggerSelectHoteluri()
	{
		window = new JFrame("Selectati Hotelul");
		SelectHoteluriFrame content = new SelectHoteluriFrame();
		window.setContentPane(content);
		window.pack();
		window.setLocation(100,100);
		window.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
		window.setVisible(true);
		window.setResizable(true);
		window.setExtendedState(JFrame.MAXIMIZED_BOTH);
	}
	
	public SelectHoteluriFrame()
	{
		this.setBackground(Color.LIGHT_GRAY);
		this.setLayout(new BorderLayout());
		
		JPanel northPanel = new JPanel();
		this.add(northPanel,BorderLayout.NORTH);
		
		JLabel message = new JLabel();
		message.setFont(new Font("SARIF",1,15));
		message.setText("Alegeti hotelul la care doriti sa stati!");
		northPanel.add(message);
		
		JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new GridBagLayout());
		centerPanel.setBackground(Color.LIGHT_GRAY);
		this.add(centerPanel,BorderLayout.CENTER);
		
		try
		{	
			while(CumparaSejurFrame.rsHoteluri.next())
			{
				JButton hotelButton = new JButton(CumparaSejurFrame.rsHoteluri.getString("Nume"));
				hotelButton.addActionListener(this);
				centerPanel.add(hotelButton);
			}
				
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		}
	
		
	}
	
	public void actionPerformed(ActionEvent evt)
	{
		hotelName = getHotel(evt.getActionCommand());
		CumparaSejurFrame.doContinue();
		window.dispose();
	}
	
	public String getHotel(String string)
	{
		return string;
	}

}
