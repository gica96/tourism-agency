import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class ObiectiveFrame extends JPanel{
	
	private static final long serialVersionUID = 1L;
	
	public static void triggerObiective()
	{
		JFrame window = new JFrame("Obiective disponibile");
		ObiectiveFrame content = new ObiectiveFrame();
		window.setContentPane(content);
		window.pack();
		window.setLocation(100,100);
		window.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
		window.setVisible(true);
		window.setResizable(true);
		window.setExtendedState(JFrame.MAXIMIZED_BOTH);
	}
	
	public ObiectiveFrame()
	{
		this.setLayout(new BorderLayout());
		this.setBackground(Color.LIGHT_GRAY);
		
		JPanel northPanel = new JPanel();
		this.add(northPanel, BorderLayout.NORTH);
		
		JLabel message = new JLabel("Mai jos, aveti lista tuturor obiectivelor turistice!");
		northPanel.add(message);
		
		Statement selectObiective;
		ResultSet rsSelectObiective;
		Statement selectDescriere;
		ResultSet rsSelectDescriere;
		Statement countObiective;
		ResultSet rsCountObiective;
		
		JPanel centerPanel = new JPanel();

		try
		{
			countObiective = Tourism_Agency.connection.createStatement();
			countObiective.execute("SELECT COUNT(obiective_turistice.id_obiectiv) FROM obiective_turistice");
			rsCountObiective = countObiective.getResultSet();
			rsCountObiective.next();
			centerPanel.setLayout(new GridLayout(rsCountObiective.getInt(1),3));
			centerPanel.setBackground(Color.LIGHT_GRAY);
			this.add(centerPanel, BorderLayout.CENTER);
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		}
		
		
		try
		{
			selectObiective = Tourism_Agency.connection.createStatement();
			selectObiective.execute("SELECT obiective_turistice.denumire FROM obiective_turistice");
			rsSelectObiective = selectObiective.getResultSet();
			
			selectDescriere = Tourism_Agency.connection.createStatement();
			selectDescriere.execute("SELECT obiective_turistice.descriere FROM obiective_turistice");
			rsSelectDescriere = selectDescriere.getResultSet();
			
			
			while(rsSelectObiective.next() && rsSelectDescriere.next())
			{
				JLabel text = new JLabel();
				text.setText(rsSelectObiective.getString("Denumire")+": ");
				centerPanel.add(text);
				
				JTextField description = new JTextField();
				description.setText(rsSelectDescriere.getString("Descriere"));
				description.setBackground(Color.LIGHT_GRAY);
				centerPanel.add(description);

			}
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		}
	}

}
